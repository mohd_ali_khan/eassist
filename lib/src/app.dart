import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/resources/colors.dart';
import 'package:teledentistry/src/resources/fonts.dart';
import 'package:teledentistry/src/ui_screens/patients_section/patient_info.dart';
import 'package:teledentistry/src/ui_screens/patients_section/video_calling.dart';
import 'package:teledentistry/src/ui_screens/splash/splash.dart';
import 'bloc/bloc_main/main_bloc.dart';
import 'ui_screens/app_drawer/account.dart';
import 'ui_screens/app_drawer/call_logs.dart';
import 'ui_screens/app_drawer/change_password.dart';
import 'ui_screens/app_drawer/consent_forms.dart';
import 'ui_screens/app_drawer/consent_forms_patient.dart';
import 'ui_screens/app_drawer/edit_account.dart';
import 'ui_screens/app_drawer/settings.dart';
import 'ui_screens/dashboard/dashboard.dart';
import 'ui_screens/login_signup/forgot_password.dart';
import 'ui_screens/login_signup/login.dart';
import 'ui_screens/login_signup/signup.dart';
import 'ui_screens/patients_section/patients_list.dart';
import 'utils/net_connectivity/connectionService.dart';
import 'utils/net_connectivity/nonetconnection.dart';
import 'utils/pagerouting/slideleftroute.dart';

class App extends StatelessWidget {
  final MainBloc mainBloc = MainBloc();

  void dispose() {
    mainBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    print(mainBloc.name);
    mainBloc.name = "app";
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: StringHelper.app_name,
      home: BlocProvider(create: (context) => mainBloc, child: Splash()),
      theme: ThemeData(
          brightness: Brightness.light,
          accentColor: ColorsHelper.whiteColor(),
          primaryColor: ColorsHelper.blueColor(),
          scaffoldBackgroundColor: ColorsHelper.whiteColor(),
          fontFamily: FontsHelper.fonts_Montserrat),
      onGenerateRoute: routes,
    );
  }

  Route routes(RouteSettings settings) {
    var page;
    String routeName = settings.name;
    switch (routeName) {
      //Splash
      case Splash.routeName:
        page = BlocProvider(create: (context) => mainBloc, child: Splash());
        break;
      //Login
      case Login.routeName:
        page = BlocProvider(create: (context) => mainBloc, child: Login());
        break;
      //Signup
      case Signup.routeName:
        page = BlocProvider(create: (context) => mainBloc, child: Signup());
        break;
      //ForgotPassword
      case ForgotPassword.routeName:
        page = BlocProvider(
            create: (context) => mainBloc, child: ForgotPassword());
        break;
      //Dashboard
      case Dashboard.routeName:
        page = BlocProvider(
            create: (context) => mainBloc,
            child: Dashboard(
              roomName: settings.arguments,
            ));
        break;
      //PatientsList
      case PatientsList.routeName:
        page = BlocProvider(
            create: (context) => mainBloc,
            child: PatientsList(
              roomName: settings.arguments,
            ));
        break;
      //PatientsInfo
      case PatientsInfo.routeName:
        page = BlocProvider(
            create: (context) => mainBloc,
            child: PatientsInfo(
              record: settings.arguments,
            ));
        break;
      //VideoCalling
      case VideoCalling.routeName:
        List<dynamic> result = settings.arguments;
        page = BlocProvider(
            create: (context) => mainBloc,
            child: VideoCalling(
              record: result[0],
              roomName: result[1],
            ));
        break;
      //CallLogs
      case CallLogs.routeName:
        page = BlocProvider(create: (context) => mainBloc, child: CallLogs());
        break;
      //ConsentForm
      case ConsentForm.routeName:
        page =
            BlocProvider(create: (context) => mainBloc, child: ConsentForm());
        break;
      //Account
      case Account.routeName:
        page = BlocProvider(create: (context) => mainBloc, child: Account());
        break;
      //Settings
      case Settings.routeName:
        page = BlocProvider(create: (context) => mainBloc, child: Settings());
        break;
      //ChangePassword
      case ChangePassword.routeName:
        page = BlocProvider(
            create: (context) => mainBloc, child: ChangePassword());
        break;
      //ChangePassword
      case EditAccount.routeName:
        page = BlocProvider(
            create: (context) => mainBloc,
            child: EditAccount(userProfileData: settings.arguments));
        break;
      //ConsentFormPatient
      case ConsentFormPatient.routeName:
        page = BlocProvider(
            create: (context) => mainBloc, child: ConsentFormPatient());
        break;
    }

    return SlideLeftRoute(
        widget: StreamBuilder(
            stream: ConnectionService.getInstance().connectionStatus,
            builder:
                (BuildContext context, AsyncSnapshot<dynamic> netConnection) {
              if (netConnection.data != null) {
                return netConnection.data ? page : NoNetConnection();
              } else {
                return NoNetConnection();
              }
            })
        // page
        );
  }
}
