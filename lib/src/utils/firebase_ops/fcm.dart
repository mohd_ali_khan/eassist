import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:teledentistry/src/utils/app_navigator/app_navigator.dart';

bool navigateToCallingScreen = false;
String roomName = '';

class Fcm {
  static Fcm fcm = Fcm();
  static bool alreadyInited = false;
  static getInstance() {
    if (fcm == null) {
      fcm = Fcm();
    }
    return fcm;
  }

  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final List<NotificationMessages> messages = [];
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  Future<String> getFbToken() async {
    var fbToken = await _firebaseMessaging.getToken();
    return fbToken;
  }

  Future<void> chatNotification({@required BuildContext context}) async {
    alreadyInited = true;
    var android = new AndroidInitializationSettings('@mipmap/ic_launcher');
    var ios = new IOSInitializationSettings();
    var platform = new InitializationSettings(android: android, iOS: ios);
    _firebaseMessaging.requestNotificationPermissions();
    flutterLocalNotificationsPlugin.initialize(platform);
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
      showNotification(message).then((onvalue) {
        return;
      });
      print('onMessage: $message');
      Future.delayed(
          Duration(
            milliseconds: 1000,
          ), () {
              print(message['notification']['data']);
        print(message['data']);
        Map<dynamic, dynamic> data = message['data'];
        print(data);
        if (data != null) {
          print(data['room_id']);
        }
        AppNavigator.launchVideoCallingScreen(
          context,
          null,
          data['room_id'],
        );
        // AppNavigator.launchVideoCallingScreen(
        //     context, null, message['notification']['title'].toString());
      });
    }, onResume: (Map<String, dynamic> message) async {
      print('onResume: $message');
       Map<dynamic, dynamic> data = message['data'];
      navigateToCallingScreen = true;
      roomName = data['room_id'];
      Future.delayed(
          Duration(
            milliseconds: 1000,
          ), () {
        print(message['notification']['data']);
        print(message['data']);
        Map<dynamic, dynamic> data = message['data'];
        print(data);
        if (data != null) {
          print(data['room_id']);
        }
        print('I was here in onResume');
        Navigator.popUntil(context, (Route<dynamic> route) => route is PageRoute);
        AppNavigator.launchVideoCallingScreen(
          context,
          null,
          data['room_id'],
        );
      });
    }, onLaunch: (Map<String, dynamic> message) async {
      print('onLaunch: $message');
      Map<dynamic, dynamic> data = message['data'];
      navigateToCallingScreen = true;
      roomName = data['room_id'];
      Future.delayed(
          Duration(
            milliseconds: 1000,
          ), () {
                print(message['notification']['data']);
        print(message['data']);
        Map<dynamic, dynamic> data = message['data'];
        print(data);
        if (data != null) {
          print(data['room_id']);
        }
                print('I was here in onLaunch');
        Navigator.popUntil(context, (Route<dynamic> route) => route is PageRoute);
        AppNavigator.launchVideoCallingScreen(
          context,
          null,
          data['room_id'],
        );
      });
    });
  }

  showNotification(Map<String, dynamic> message) async {
    var android = AndroidNotificationDetails(
        'channelId', 'channelName', 'channelDescription');
    var iOS = IOSNotificationDetails();
    var platform = NotificationDetails(android: android, iOS: iOS);
    final notification = message['notification'];
    await flutterLocalNotificationsPlugin.show(
        0, notification['title'], notification['body'], platform);
  }
}

@immutable
class NotificationMessages {
  final String title;
  final String body;

  const NotificationMessages({
    @required this.title,
    @required this.body,
  });
}
