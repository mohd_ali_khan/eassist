import 'package:flutter/material.dart';
import 'package:teledentistry/src/models/SearchListModel.dart';
import 'package:teledentistry/src/models/UserProfileModel.dart';
import 'package:teledentistry/src/ui_screens/app_drawer/account.dart';
import 'package:teledentistry/src/ui_screens/app_drawer/call_logs.dart';
import 'package:teledentistry/src/ui_screens/app_drawer/change_password.dart';
import 'package:teledentistry/src/ui_screens/app_drawer/consent_forms.dart';
import 'package:teledentistry/src/ui_screens/app_drawer/consent_forms_patient.dart';
import 'package:teledentistry/src/ui_screens/app_drawer/edit_account.dart';
import 'package:teledentistry/src/ui_screens/app_drawer/settings.dart';
import 'package:teledentistry/src/ui_screens/dashboard/dashboard.dart';
import 'package:teledentistry/src/ui_screens/login_signup/forgot_password.dart';
import 'package:teledentistry/src/ui_screens/login_signup/login.dart';
import 'package:teledentistry/src/ui_screens/login_signup/signup.dart';
import 'package:teledentistry/src/ui_screens/patients_section/patient_info.dart';
import 'package:teledentistry/src/ui_screens/patients_section/patients_list.dart';
import 'package:teledentistry/src/ui_screens/patients_section/video_calling.dart';
import 'package:teledentistry/src/ui_screens/splash/splash.dart';

class AppNavigator {
  //Teledentistry

  //Splash
  static void launchSplashScreen(BuildContext context) {
    Navigator.of(context).pushNamedAndRemoveUntil(
        Splash.routeName, (Route<dynamic> route) => false);
  }

  //Dashboard
  static void launchDashboardScreen(BuildContext context, String roomName) {
    Navigator.of(context).pushNamedAndRemoveUntil(
        Dashboard.routeName, (Route<dynamic> route) => false,
        arguments: roomName);
  }

  //Login
  static void launchLoginScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      Login.routeName,
    );
  }

  //Signup
  static void launchSignupScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      Signup.routeName,
    );
  }

  //ForgotPassword
  static void launchForgotPasswordScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      ForgotPassword.routeName,
    );
  }

  //PatientsList
  static void launchPatientsListScreen(BuildContext context, String roomName) {
    Navigator.pushNamed(context, PatientsList.routeName, arguments: roomName);
  }

  //PatientsInfo
  static void launchPatientsInfoScreen(BuildContext context, Record record) {
    Navigator.pushNamed(context, PatientsInfo.routeName, arguments: record);
  }

  //VideoCalling
  static void launchVideoCallingScreen(
      BuildContext context, Record record, String roomName) {
    Navigator.pushNamed(context, VideoCalling.routeName,
        arguments: [record, roomName]);
  }

  //CallLogs
  static void launchCallLogsScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      CallLogs.routeName,
    );
  }

  //ConsentForm
  static void launchConsentFormScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      ConsentForm.routeName,
    );
  }

  //Account
  static void launchAccountScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      Account.routeName,
    );
  }

  //Settings
  static void launchSettingsScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      Settings.routeName,
    );
  }

  //ChangePassword
  static void launchChangePasswordScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      ChangePassword.routeName,
    );
  }

  //EditAccount
  static void launchEditAccountScreen(
      BuildContext context, UserProfileData userProfileData) {
    Navigator.pushNamed(context, EditAccount.routeName,
        arguments: userProfileData);
  }

  //ConsentFormPatient
  static void launchConsentFormPatientScreen(BuildContext context) {
    Navigator.pushNamed(
      context,
      ConsentFormPatient.routeName,
    );
  }

  static void popBackStack(BuildContext context) {
    Navigator.pop(context);
  }
}
