import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:teledentistry/src/resources/colors.dart';
import 'package:teledentistry/src/resources/image_assets.dart';
import 'package:teledentistry/src/utils/buttons/button.dart';

callUnfocus({@required BuildContext context}) =>
    FocusScope.of(context).requestFocus(new FocusNode());

Widget textHelper(
        {String title,
        Color textColor = Colors.black,
        double fontSize,
        bool isItalic = false,
        FontWeight fontWeight = FontWeight.normal,
        TextAlign textAlign = TextAlign.center}) =>
    Text(
      title,
      style: TextStyle(
          fontSize: fontSize,
          color: textColor,
          fontWeight: fontWeight,
          fontStyle: isItalic ? FontStyle.italic : FontStyle.normal),
      textAlign: textAlign,
    );

Widget assetImageHelper({String image, double height, double width}) =>
    Container(
      height: height,
      width: width,
      child: Image.asset(
        image,
        fit: BoxFit.fill,
      ),
    );

Widget appBarBody(
        {String title,
        Color textColor,
        double fontSize = 15,
        FontWeight fontWeight = FontWeight.normal,
        bool isCenter = true,
        bool isLeading = false,
        String leadingIcon,
        Function onTapLeading,
        Color leadingColor = const Color(0xff8D98A4),
        String action1Icon,
        Function onTapaction1,
        String action2Icon,
        Function onTapaction2,
        Color bgColor = Colors.transparent,
        double elevation = 0}) =>
    AppBar(
      brightness: Brightness.light,
      backgroundColor: bgColor,
      elevation: elevation,
      centerTitle: isCenter,
      //title
      title: Text(
        title,
        style: TextStyle(
            fontSize: fontSize,
            color: textColor ?? Colors.black,
            fontWeight: fontWeight),
        textAlign: TextAlign.center,
      ),
      automaticallyImplyLeading: isLeading,
      //leading Icon
      leading: isLeading
          ? InkWell(
              onTap: onTapLeading,
              child: leadingIcon != null
                  ? Container(
                      margin: EdgeInsets.all(13),
                      child: Image.asset(
                        leadingIcon,
                      ))
                  : Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 5, vertical: 10),
                      child: Icon(
                        Icons.arrow_back_ios,
                        color: leadingColor,
                      ),
                    ),
            )
          : null,
      //Action icon
      actions: <Widget>[
        action1Icon != null
            ? InkWell(
                borderRadius: BorderRadius.circular(30),
                onTap: onTapaction1,
                child: Container(
                    width: 35,
                    padding: EdgeInsets.all(5),
                    child: Image.asset(
                      action1Icon,
                    )),
              )
            : Container(
                height: 0,
                width: 0,
              ),
        SizedBox(
          width: 5,
        ),
        action2Icon != null
            ? InkWell(
                borderRadius: BorderRadius.circular(50),
                onTap: onTapaction2,
                child: Container(
                    width: 35,
                    padding: EdgeInsets.all(5),
                    child: Image.asset(
                      action2Icon,
                    )),
              )
            : Container(
                height: 0,
                width: 0,
              ),
        SizedBox(
          width: 15,
        )
      ],
    );

Widget textBoxContainer({Widget child, double vertical = 0}) => Container(
    padding: EdgeInsets.symmetric(horizontal: 10, vertical: vertical),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: ColorsHelper.greyTextBoxColor()),
    child: child);

Widget formTextFeild(
        {String hintText,
        String validation(String val),
        String save(String val),
        void onChanged(String val),
        TextEditingController controller,
        FocusNode focusNode,
        FocusNode nextFocusNode,
        textInputType = TextInputType.text,
        textCapitalization = TextCapitalization.sentences,
        TextInputAction textInputAction,
        int maxLine = 1,
        int maxLength = 150,
        bool obscureText = false,
        bool suffixIcon = false,
        Function suffixFuncton,
        BuildContext context}) =>
    TextFormField(
      scrollPadding: EdgeInsets.all(0),
      maxLines: maxLine,
      inputFormatters: [
        LengthLimitingTextInputFormatter(maxLength),
      ],
      onChanged: onChanged,
      validator: validation,
      onSaved: save,
      obscureText: obscureText,
      controller: controller,
      focusNode: focusNode,
      keyboardType: textInputType,
      textInputAction: textInputAction,
      onFieldSubmitted: (str) => {
        nextFocusNode != null
            ? FocusScope.of(context).requestFocus(nextFocusNode)
            : FocusScope.of(context).requestFocus(new FocusNode()),
      },
      decoration: InputDecoration(
        hintMaxLines: 2,
        labelStyle: TextStyle(fontSize: 15),
        labelText: hintText,
        border: UnderlineInputBorder(),
        suffixIcon: suffixIcon
            ? Container(
                child: Transform.scale(
                  scale: 0.65,
                  child: IconButton(
                    onPressed: suffixFuncton,
                    icon: assetImageHelper(
                        image: obscureText
                            ? ImageAssets.password_show
                            : ImageAssets.password_hide),
                  ),
                ),
              )
            : null,
      ),
    );

Widget submitButton({
  String title,
  Function onPressed,
}) =>
    ButtonColor(
        color: ColorsHelper.blueColor(),
        radius: 10,
        onPressed: onPressed,
        child: Text(
          title,
          style: TextStyle(
              color: ColorsHelper.whiteColor(), fontWeight: FontWeight.w600),
        ));

Widget textWithLink({
  String text1,
  String text2,
  Function ontap,
  double fontSize = 13,
}) =>
    Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        //text1
        Flexible(
          child: textHelper(
            textColor: ColorsHelper.greyColor(),
            title: text1,
            fontSize: fontSize,
          ),
        ),
        //text2
        InkWell(
            borderRadius: BorderRadius.circular(5),
            onTap: ontap,
            child: Container(
                padding: EdgeInsets.all(5),
                child: Container(
                  // decoration: BoxDecoration(
                  //     border: Border(
                  //         bottom: BorderSide(
                  //   color: ColorsHelper.whiteColor(),
                  //   width: 1.0,
                  // ))),
                  child: textHelper(
                      title: text2,
                      textColor: ColorsHelper.lightBlueColor(),
                      fontWeight: FontWeight.w700,
                      fontSize: fontSize),
                ))),
      ],
    );

Widget cacheImage({String imgUrl}) => CachedNetworkImage(
      fit: BoxFit.cover,
      imageUrl: imgUrl,
      progressIndicatorBuilder: (context, url, downloadProgress) => Container(
        child: Center(
          child: CircularProgressIndicator(value: downloadProgress.progress),
        ),
      ),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );

Widget line({double margin = 10}) => //Line
    Container(
      margin: EdgeInsets.symmetric(vertical: margin),
      color: ColorsHelper.greyColor(),
      width: double.infinity,
      height: 1,
    );

Widget bottonNavBar({@required BuildContext context}) => Container(
      margin: EdgeInsets.symmetric(
          vertical: 20, horizontal: MediaQuery.of(context).size.width / 3),
      // height: MediaQuery.of(context).size.height * 0.085,
      width: 200,
      child: assetImageHelper(
        image: ImageAssets.group_1,
      ),
    );
