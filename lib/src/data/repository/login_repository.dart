import 'dart:io';

import 'package:flutter/material.dart';
import 'package:teledentistry/src/data/network/api.dart';
import 'package:teledentistry/src/data/network/api_response.dart';
import 'package:teledentistry/src/data/network/rest_client_new.dart';
import 'package:teledentistry/src/models/LoginModel.dart';
import 'package:teledentistry/src/models/RegisterModel.dart';
import 'package:teledentistry/src/models/UrlLinksModel.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/utils/loading_widget.dart/loading_widget.dart';
import 'package:teledentistry/src/utils/popup_dialogs/popup_dialogs.dart';
import 'package:teledentistry/src/utils/sharedpreference_helper/sharepreference_helper.dart';

class LoginRepository {
  //Login
  static Future<LoginData> loginApiCall(
      {String email,
      String password,
      String deviceId,
      BuildContext context}) async {
    var body = {
      "email": email,
      "password": password,
    };
    LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse =
          await restClientNew.post(context, Api.login, body: body, header: {
        'version': '1.0.0',
        'device_id': deviceId,
        'device_type': Platform.isIOS ? 'ios' : 'android'
      });
      if (apiResponse.status == 1) {
        LoginData loginData;
        loginData = LoginData.fromJson(apiResponse.data);
        LoadingWidget.endLoadingWidget(context);
        return loginData;
      } else {
        LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      PopupDialogs.displayMessage(context, StringHelper.aPI_Crashed);
      return null;
    }
  }

  //registration
  static Future<RegisterData> registrationApiCall(
      {String fullName,
      String email,
      String password,
      String type,
      String phoneNo,
      BuildContext context}) async {
    var body = {
      "type": type,
      "email": email,
      "password": password,
      "full_name": fullName,
      "user_phone": phoneNo,
    };
    LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse =
          await restClientNew.post(context, Api.register, body: body, header: {
        'version': '1.0.0',
      });
      if (apiResponse.status == 1) {
        RegisterData registerData;
        registerData = RegisterData.fromJson(apiResponse.data);
        LoadingWidget.endLoadingWidget(context);
        return registerData;
      } else {
        LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      PopupDialogs.displayMessage(context, StringHelper.aPI_Crashed);
      return null;
    }
  }

  //forgetPassword
  static Future<dynamic> forgetPasswordApiCall(
      {String email, BuildContext context}) async {
    var body = {
      "email": email,
    };
    LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse = await restClientNew
          .post(context, Api.forgotpassword, body: body, header: {
        'version': '1.0.0',
      });
      if (apiResponse.status == 1) {
        LoadingWidget.endLoadingWidget(context);
        return apiResponse.message;
      } else {
        LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      PopupDialogs.displayMessage(context, StringHelper.aPI_Crashed);
      return null;
    }
  }

  //Url's Link
  static Future<UrlLinksData> urlLinksApiCall(
      {String apiLink, BuildContext context}) async {
    try {
      ApiResponse apiResponse = await restClientNew.get(
        context,
        apiLink,
      );
      if (apiResponse.status == 1) {
        UrlLinksData urlLinksData;
        urlLinksData = UrlLinksData.fromJson(apiResponse.data);

        return urlLinksData;
      } else {
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      PopupDialogs.displayMessage(context, StringHelper.aPI_Crashed);
      return null;
    }
  }

  //ChangePassword
  static Future<dynamic> changePasswordApiCall(
      {String oldPassword, String newPassword, BuildContext context}) async {
    var body = {
      "old_password": oldPassword,
      "new_password": newPassword,
    };
    var token = await SharePreferencesHelper.getInstant()
        .getAccessToken(SharePreferencesHelper.Access_Token);
    LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse = await restClientNew.post(
          context, Api.changepassword,
          body: body, header: {'version': '1.0.0', 'Authorization': token});
      if (apiResponse.status == 1) {
        LoadingWidget.endLoadingWidget(context);
        return apiResponse.message;
      } else {
        LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      PopupDialogs.displayMessage(context, StringHelper.aPI_Crashed);
      return null;
    }
  }

  //Social Login
  static Future<Login> socialLoginApiCall(
      {String type,
      String socialId,
      String fullName,
      String deviceId,
      String email,
      BuildContext context}) async {
    var body = {
      "type": type,
      "social_id": socialId,
      "full_name": fullName,
      "email": email
    };
    LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse = await restClientNew
          .post(context, Api.social_login, body: body, header: {
        'version': '1.0.0',
        'device_id': deviceId,
        'device_type': Platform.isIOS ? 'ios' : 'android'
      });
      if (apiResponse.status == 1) {
        Login loginData;
        loginData = Login.fromJson(apiResponse.data);
        LoadingWidget.endLoadingWidget(context);
        return loginData;
      } else {
        LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      PopupDialogs.displayMessage(context, StringHelper.aPI_Crashed);
      return null;
    }
  }
}
