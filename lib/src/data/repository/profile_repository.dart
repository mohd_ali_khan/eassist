import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:teledentistry/src/data/network/api.dart';
import 'package:teledentistry/src/data/network/api_response.dart';
import 'package:teledentistry/src/data/network/rest_client_new.dart';
import 'package:teledentistry/src/models/UserProfileModel.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/utils/loading_widget.dart/loading_widget.dart';
import 'package:teledentistry/src/utils/popup_dialogs/popup_dialogs.dart';
import 'package:teledentistry/src/utils/sharedpreference_helper/sharepreference_helper.dart';

class ProfileRepository {
  //Get User Profile
  static Future<UserProfileData> getUserProfileApiCall(
      {BuildContext context}) async {
    try {
      var token = await SharePreferencesHelper.getInstant()
          .getAccessToken(SharePreferencesHelper.Access_Token);
      ApiResponse apiResponse = await restClientNew.get(
          context, Api.get_profile,
          header: {'version': '1.0.0', 'Authorization': token});
      if (apiResponse.status == 1) {
        UserProfileData userProfileData;
        userProfileData = UserProfileData.fromJson(apiResponse.data);

        return userProfileData;
      } else {
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      PopupDialogs.displayMessage(context, StringHelper.aPI_Crashed);
      return null;
    }
  }

  //Profile Update
  static Future<dynamic> profileUpdateApiCall(
      {File profileImage,
      String fullName,
      String weight,
      String gender,
      String height,
      String dob,
      String insurance,
      String policy,
      BuildContext context}) async {
    // var body = {
    //   "profile_image": profileImage != null
    //       ? await MultipartFile.fromFile(profileImage.path,
    //           filename: profileImage.path.split("/").last)
    //       : null,
    //   "full_name": fullName,
    //   "weight": weight,
    //   "gender": gender,
    //   "height": height,
    //   "dob": dob,
    //   "insurance": insurance,
    //   "policy": policy,
    // };
    FormData formData = FormData.fromMap({
      "profile_image": profileImage != null
          ? await MultipartFile.fromFile(profileImage.path,
              filename: profileImage.path.split("/").last)
          : null,
      "full_name": fullName,
      "weight": weight,
      "gender": gender,
      "height": height,
      "dob": dob,
      "insurance": insurance,
      "policy": policy,
    });
    LoadingWidget.startLoadingWidget(context);
    var token = await SharePreferencesHelper.getInstant()
        .getAccessToken(SharePreferencesHelper.Access_Token);
    try {
      ApiResponse apiResponse = await restClientNew.post(
          context, Api.profile_update,
          body: formData, header: {'version': '1.0.0', 'Authorization': token});
      if (apiResponse.status == 1) {
        LoadingWidget.endLoadingWidget(context);
        return apiResponse.message;
      } else {
        LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      PopupDialogs.displayMessage(context, StringHelper.aPI_Crashed);
      return null;
    }
  }
}
