import 'dart:io';

import 'package:flutter/material.dart';
import 'package:teledentistry/src/data/network/api.dart';
import 'package:teledentistry/src/data/network/api_response.dart';
import 'package:teledentistry/src/data/network/rest_client_new.dart';
import 'package:teledentistry/src/models/GetRoomsModel.dart';
import 'package:teledentistry/src/models/VersionCheckModel.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/utils/loading_widget.dart/loading_widget.dart';
import 'package:teledentistry/src/utils/popup_dialogs/popup_dialogs.dart';
import 'package:teledentistry/src/utils/sharedpreference_helper/sharepreference_helper.dart';

class CommonRepository {
  // //Check App version
  // static Future<dynamic> checkAppVersionApiCall({BuildContext context}) async {
  //   try {
  //     ApiResponse apiResponse = await restClientNew.get(
  //       context,
  //       Api,
  //     );
  //     if (apiResponse.) {
  //       return apiResponse.data;
  //     } else {
  //       PopupDialogs.displayMessage(context, apiResponse.message);
  //       return null;
  //     }
  //   } catch (e) {
  //     PopupDialogs.displayMessage(context, StringHelper.aPI_Crashed);
  //     return null;
  //   }
  // }

  //Send notifications
  static Future<dynamic> sendNotificationsApiCall(
      {String receiverId, String roomName, BuildContext context}) async {
    var body = {"user_id": receiverId, "room_name": roomName};
    var token = await SharePreferencesHelper.getInstant()
        .getAccessToken(SharePreferencesHelper.Access_Token);
    LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse = await restClientNew.post(
          context, Api.send_notification,
          body: body, header: {'version': '1.0.0', 'Authorization': token});
      if (apiResponse.status == 1) {
        LoadingWidget.endLoadingWidget(context);
        return apiResponse.message;
      } else {
        LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      PopupDialogs.displayMessage(context, StringHelper.aPI_Crashed);
      return null;
    }
  }

  //Send Call Logs
  static Future<dynamic> sendCallLogsApiCall(
      {String callerUserId,
      String receiverUserId,
      String dtOfCall,
      String timeOfCall,
      String endOfCall,
      String purposeCall,
      String followupNeeded,
      BuildContext context}) async {
    var body = {
      "caller_user_id": callerUserId,
      "receiver_user_id": receiverUserId,
      "dt_of_call": dtOfCall,
      "time_of_call": timeOfCall,
      "end_of_call": endOfCall,
      "purpose_call": "Tooth Checkup",
      "followup_needed": "0"
    };
    var token = await SharePreferencesHelper.getInstant()
        .getAccessToken(SharePreferencesHelper.Access_Token);
    LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse = await restClientNew.post(
          context, Api.log_submit,
          body: body, header: {'version': '1.0.0', 'Authorization': token});
      if (apiResponse.status == 1) {
        LoadingWidget.endLoadingWidget(context);
        return apiResponse.message;
      } else {
        LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      PopupDialogs.displayMessage(context, StringHelper.aPI_Crashed);
      return null;
    }
  }

  //Get Rooms
  static Future<GetRoomsData> getRoomsApiCall({BuildContext context}) async {
    var token = await SharePreferencesHelper.getInstant()
        .getAccessToken(SharePreferencesHelper.Access_Token);

    try {
      ApiResponse apiResponse = await restClientNew.get(context, Api.get_room,
          header: {'version': '1.0.0', 'Authorization': token});
      if (apiResponse.status == 1) {
        GetRoomsData getRoomsData;
        getRoomsData = GetRoomsData.fromJson(apiResponse.data);
        return getRoomsData;
      } else {
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      PopupDialogs.displayMessage(context, StringHelper.aPI_Crashed);
      return null;
    }
  }

  //Remove Room
  static Future<dynamic> removeRoomApiCall(
      {String receiverId, BuildContext context}) async {
    var token = await SharePreferencesHelper.getInstant()
        .getAccessToken(SharePreferencesHelper.Access_Token);
    var id = await SharePreferencesHelper.getInstant()
        .getString(SharePreferencesHelper.Id);
    var body = {"user_id": receiverId ?? id};
    LoadingWidget.startLoadingWidget(context);
    try {
      ApiResponse apiResponse = await restClientNew.post(
          context, Api.remove_room,
          body: body, header: {'version': '1.0.0', 'Authorization': token});
      if (apiResponse.status == 1) {
        LoadingWidget.endLoadingWidget(context);
        return apiResponse.message;
      } else {
        LoadingWidget.endLoadingWidget(context);
        PopupDialogs.displayMessage(context, apiResponse.message);
        return null;
      }
    } catch (e) {
      LoadingWidget.endLoadingWidget(context);
      PopupDialogs.displayMessage(context, StringHelper.aPI_Crashed);
      return null;
    }
  }

  //Version Check
  static Future<VersionCheckData> versionCheckApiCall(
      {BuildContext context}) async {
    try {
      ApiResponse apiResponse = await restClientNew.get(context, Api.version,
          header: {
            'version': '1.0.0',
            'device_type': Platform.isIOS ? 'ios' : 'android'
          });
      if (apiResponse.data != null) {
        VersionCheckData versionCheckData;
        versionCheckData = VersionCheckData.fromJson(apiResponse.data);
        return versionCheckData;
      } else {
        PopupDialogs.displayMessage(context, StringHelper.aPI_Crashed);
        return null;
      }
    } catch (e) {
      PopupDialogs.displayMessage(context, StringHelper.aPI_Crashed);
      return null;
    }
  }
}
