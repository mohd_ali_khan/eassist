class Api {
  static const BASE_URL = "http://knowledgerevise.com/eAssist/webservices";

  static const login = "/users/login";
  static const social_login = "/users/social_login";
  static const register = "/users/register";
  static const logout = "/users/logout";
  static const forgotpassword = "/users/forgotpassword";
  static const changepassword = "/users/changepassword";

  //
  static const privacy_policy = "/static_page/privacy_policy";
  static const consent_form = "/static_page/consent_form";
  static const terms_condition = "/static_page/terms_condition";

  //
  static const get_profile = "/users/get_profile";
  static const profile_update = "/users/profile_update";

  //
  static const search = "/users/search";
  static const send_notification = "/log/notification";

  //
  static const get_room = "/log/get_room";
  static const remove_room = "/log/remove_room";
  static const get_log = "/log/get_log";
  static const log_submit = "/log/log_submit";

  //
  static const version = "/users/version";
}
