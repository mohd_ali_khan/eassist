class ApiResponse {
  Version version;
  int status;
  int errorcode;
  String message;
  dynamic data;

  ApiResponse(
      {this.version, this.status, this.errorcode, this.message, this.data});

  ApiResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    errorcode = json['errorcode'];
    message = json['message'];
    data = json['data'];
    version =
        json['version'] != null ? new Version.fromJson(json['version']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.version != null) {
      data['version'] = this.version.toJson();
    }
    data['status'] = this.status;
    data['errorcode'] = this.errorcode;
    data['message'] = this.message;
    data['data'] = this.data;

    return data;
  }
}

class Version {
  int status;
  int versioncode;
  String versionmessage;
  String currentVersion;

  Version(
      {this.status,
      this.versioncode,
      this.versionmessage,
      this.currentVersion});

  Version.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    versioncode = json['versioncode'];
    versionmessage = json['versionmessage'];
    currentVersion = json['current_version'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['versioncode'] = this.versioncode;
    data['versionmessage'] = this.versionmessage;
    data['current_version'] = this.currentVersion;
    return data;
  }
}
