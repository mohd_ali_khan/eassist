import 'dart:async';
import 'dart:collection';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/utils/app_navigator/app_navigator.dart';
import 'package:teledentistry/src/utils/sharedpreference_helper/sharepreference_helper.dart';
import 'api.dart';
import 'api_response.dart';

class RestClientNew {
  Dio _dio;

  RestClientNew() {
    BaseOptions options = new BaseOptions(
      baseUrl: Api.BASE_URL,
    );
    _dio = Dio(options);
    _dio.interceptors
        .add(LogInterceptor(requestBody: true, responseBody: true));
  }

  Future<ApiResponse> get(BuildContext context, apiName,
      {body, Map<String, dynamic> header, Options option}) async {
    Map<String, dynamic> headers = HashMap();

    if (header != null) {
      headers.addAll(header);
    }

    if (option != null) {
      option.headers = headers;
    } else {
      option = Options(method: "get");
      option.headers = headers;
    }

    try {
      Response response =
          await _dio.request(apiName, data: body, options: option);
      ApiResponse apiResponse = ApiResponse.fromJson(response.data);
      if (apiResponse.status == 401) {
        doLogout(context);
      }
      return ApiResponse.fromJson(response.data);
    } catch (error) {
      if (error.response.statusCode == 400) {
        return ApiResponse(
          status: 0,
          message: StringHelper.server_error_please_try_again_later,
          data: null,
        );
      }
      return ApiResponse(
        status: 0,
        message: StringHelper.server_error_please_try_again_later,
        data: null,
      );
    }
  }

  Future<ApiResponse> post(
    BuildContext context,
    apiName, {
    body,
    Map<String, dynamic> header,
    Options option,
  }) async {
    Map<String, dynamic> headers = HashMap();

    if (header != null) {
      headers.addAll(header);
    }

    if (option != null) {
      option.headers = headers;
    } else {
      option = Options(method: "post");
      option.headers = headers;
    }
    Response response;
    try {
      response = await _dio.request(
        apiName,
        data: body,
        options: option,
        onSendProgress: (int sent, int total) {
          print("Sent  = " +
              sent.toString() +
              " Left = " +
              (total - sent).toString());
        },
      );
      print(response.data);
      ApiResponse apiResponse = ApiResponse.fromJson(response.data);
      if (apiResponse.status == 401) {
        doLogout(context);
      }
      return ApiResponse.fromJson(response.data);
    } catch (e) {
      var error = e as DioError;
      if (error.response.statusCode == 404) {
        return ApiResponse(
          status: 0,
          message: StringHelper.server_error_please_try_again_later,
          data: null,
        );
      }
      return ApiResponse(
        status: 0,
        message: StringHelper.server_error_please_try_again_later,
        data: null,
      );
    }
  }

  Future<ApiResponse> put(BuildContext context, apiName,
      {body, Map<String, dynamic> header, Options option}) async {
    Map<String, dynamic> headers = HashMap();

    if (header != null) {
      headers.addAll(header);
    }

    if (option != null) {
      option.headers = headers;
    } else {
      option = Options(method: "put");
      option.headers = headers;
    }

    try {
      Response response =
          await _dio.request(apiName, data: body, options: option);

      return ApiResponse.fromJson(response.data);
    } catch (e) {
      var error = e as DioError;
      if (error.response.statusCode == 401) {
        // doLogout(context);
        //   AppNavigator.launchAuthFailedPage(context);
      }
      return ApiResponse(
        status: 0,
        message: StringHelper.server_error_please_try_again_later,
        data: null,
      );
    }
  }

  void doLogout(BuildContext context) async {
    SharePreferencesHelper.getInstant().clearPreference();
    SharePreferencesHelper.getInstant()
        .setBool(SharePreferencesHelper.Is_Login, false);
    AppNavigator.launchLoginScreen(context);
  }
}

final RestClientNew restClientNew = RestClientNew();
