import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:teledentistry/src/data/repository/login_repository.dart';
import 'package:teledentistry/src/models/LoginModel.dart';
import 'package:teledentistry/src/models/RegisterModel.dart';
import 'package:teledentistry/src/models/UrlLinksModel.dart';
import 'package:teledentistry/src/utils/sharedpreference_helper/sharepreference_helper.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  bool autoValidation = false;

  LoginBloc() : super(null);

  //onLoginClick
  Future<bool> onLoginClick(final formKey) async {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      return true;
    } else {
      autoValidation = true;
      return false;
    }
  }

  //Login
  Future<bool> callLoginApi(
      {String email,
      String password,
      String deviceId,
      BuildContext context}) async {
    LoginData result = await LoginRepository.loginApiCall(
        email: email, password: password, deviceId: deviceId, context: context);
    if (result != null) {
      SharePreferencesHelper.getInstant().setString(
          SharePreferencesHelper.Access_Token,
          result.login.authorization ?? null);
      SharePreferencesHelper.getInstant()
          .setString(SharePreferencesHelper.Id, result.login.userId ?? null);
      SharePreferencesHelper.getInstant().setString(
          SharePreferencesHelper.Email, result.login.userEmail ?? null);
      SharePreferencesHelper.getInstant().setString(
          SharePreferencesHelper.Full_Name, result.login.fullName ?? null);
      SharePreferencesHelper.getInstant().setString(
          SharePreferencesHelper.Profile_Pic, result.login.userPic ?? null);
      SharePreferencesHelper.getInstant().setString(
          SharePreferencesHelper.User_role, result.login.userRole ?? null);
      return true;
    } else {
      return false;
    }
  }

  //registration
  Future<bool> callRegistrationApi(
      {String fullName,
      String email,
      String password,
      String type,
      String phoneNo,
      String deviceId,
      BuildContext context}) async {
    RegisterData result = await LoginRepository.registrationApiCall(
        fullName: fullName,
        email: email,
        password: password,
        type: type,
        phoneNo: phoneNo,
        context: context);
    if (result != null) {
      SharePreferencesHelper.getInstant()
          .setString(SharePreferencesHelper.Id, result.register.userId ?? null);
      LoginBloc loginBloc = LoginBloc();
      var islogin = await loginBloc.callLoginApi(
          email: email,
          password: password,
          deviceId: deviceId,
          context: context);
      loginBloc.close();
      return islogin;
    } else {
      return false;
    }
  }

  //forgetPassword
  Future<dynamic> callForgetPasswordApi(
      {String email, BuildContext context}) async {
    var result = await LoginRepository.forgetPasswordApiCall(
        email: email, context: context);
    return result;
  }

  //UrlLink
  Future<UrlLinksData> callUrlLinksApi(
      {String apiLink, BuildContext context}) async {
    UrlLinksData result = await LoginRepository.urlLinksApiCall(
        apiLink: apiLink, context: context);
    return result;
  }

  //ChangePassword
  Future<dynamic> callChangePasswordApi(
      {String oldPassword, String newPassword, BuildContext context}) async {
    var result = await LoginRepository.changePasswordApiCall(
        oldPassword: oldPassword, newPassword: newPassword, context: context);
    return result;
  }

  //Social Login
  Future<bool> callSocialLoginApi(
      {String type,
      String socialId,
      String fullName,
      String deviceId,
      String email,
      BuildContext context}) async {
    Login result = await LoginRepository.socialLoginApiCall(
        type: type,
        socialId: socialId,
        fullName: fullName,
        deviceId: deviceId,
        email: email,
        context: context);
    if (result != null) {
      SharePreferencesHelper.getInstant().setString(
          SharePreferencesHelper.Access_Token, result.authorization ?? null);
      SharePreferencesHelper.getInstant()
          .setString(SharePreferencesHelper.Id, result.userId ?? null);
      SharePreferencesHelper.getInstant()
          .setString(SharePreferencesHelper.Email, result.userEmail ?? null);
      SharePreferencesHelper.getInstant()
          .setString(SharePreferencesHelper.Full_Name, result.fullName ?? null);
      SharePreferencesHelper.getInstant().setString(
          SharePreferencesHelper.Profile_Pic, result.userPic ?? null);
      SharePreferencesHelper.getInstant()
          .setString(SharePreferencesHelper.User_role, result.userRole ?? null);
      return true;
    } else {
      return false;
    }
  }

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {}
}
