import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:teledentistry/src/data/repository/common_repository.dart';
import 'package:teledentistry/src/models/GetRoomsModel.dart';
import 'package:teledentistry/src/models/VersionCheckModel.dart';

part 'main_event.dart';
part 'main_state.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  String name = "Ram";
  String fbToken;
  String userType;
  bool isCallTaken = false;

  MainBloc() : super(null);

  //SendNotifications
  Future<dynamic> callSendNotificationsApi(
      {String receiverId, String roomName, BuildContext context}) async {
    var result = await CommonRepository.sendNotificationsApiCall(
        receiverId: receiverId, roomName: roomName, context: context);
    return result;
  }

  //GetRooms
  Future<GetRoomsData> callGetRoomsApi({BuildContext context}) async {
    var result = await CommonRepository.getRoomsApiCall(context: context);
    return result;
  }

  //Remove Room
  Future<dynamic> callRemoveRoomApi(
      {String receiverId, BuildContext context}) async {
    var result = await CommonRepository.removeRoomApiCall(
        receiverId: receiverId, context: context);
    return result;
  }

  //SendCallLogs
  Future<dynamic> callSendCallLogsApi(
      {String callerUserId,
      String receiverUserId,
      String dtOfCall,
      String timeOfCall,
      String endOfCall,
      String purposeCall,
      String followupNeeded,
      BuildContext context}) async {
    var result = await CommonRepository.sendCallLogsApiCall(
        callerUserId: callerUserId,
        receiverUserId: receiverUserId,
        dtOfCall: dtOfCall,
        timeOfCall: timeOfCall,
        endOfCall: endOfCall,
        purposeCall: purposeCall,
        followupNeeded: followupNeeded,
        context: context);
    return result;
  }

  //VersionCheck
  Future<VersionCheckData> callVersionCheckApi({BuildContext context}) async {
    var result = await CommonRepository.versionCheckApiCall(context: context);
    return result;
  }

  getIntialData(BuildContext context) async {}

  Future tp() async {}

  @override
  Stream<MainState> mapEventToState(
    MainEvent event,
  ) async* {}
}
