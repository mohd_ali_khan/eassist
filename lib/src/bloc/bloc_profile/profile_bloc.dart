import 'dart:async';
import 'dart:io';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:teledentistry/src/data/repository/profile_repository.dart';
import 'package:teledentistry/src/models/UserProfileModel.dart';
import 'package:teledentistry/src/utils/sharedpreference_helper/sharepreference_helper.dart';
part 'profile_event.dart';
part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc() : super(null);

  //onLoginClick
  Future<bool> onLoginClick(final formKey) async {
    if (formKey.currentState.validate()) {
      formKey.currentState.save();
      return true;
    } else {
      return false;
    }
  }

  //FetUserProfile
  Future<UserProfileData> callGetUserProfileApi({BuildContext context}) async {
    UserProfileData result =
        await ProfileRepository.getUserProfileApiCall(context: context);
    if (result != null) {
      SharePreferencesHelper.getInstant()
          .setString(SharePreferencesHelper.Id, result.profile.id ?? null);
      SharePreferencesHelper.getInstant().setString(
          SharePreferencesHelper.Email, result.profile.userEmail ?? null);
      SharePreferencesHelper.getInstant().setString(
          SharePreferencesHelper.Full_Name, result.profile.fullName ?? null);
      SharePreferencesHelper.getInstant().setString(
          SharePreferencesHelper.Profile_Pic, result.profile.userPic ?? null);
      SharePreferencesHelper.getInstant().setString(
          SharePreferencesHelper.User_role, result.profile.userRole ?? null);
      return result;
    } else {
      return result;
    }
  }

  //Profile Update
  Future<dynamic> callProfileUpdateApi(
      {File profileImage,
      String fullName,
      String weight,
      String gender,
      String height,
      String dob,
      String insurance,
      String policy,
      BuildContext context}) async {
    var result = await ProfileRepository.profileUpdateApiCall(
        profileImage: profileImage,
        fullName: fullName,
        weight: weight,
        gender: gender,
        height: height,
        dob: dob,
        insurance: insurance,
        policy: policy,
        context: context);
    return result;
  }

  @override
  Stream<ProfileState> mapEventToState(
    ProfileEvent event,
  ) async* {}
}
