import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:teledentistry/src/bloc/bloc_main/main_bloc.dart';
import 'package:teledentistry/src/models/GetRoomsModel.dart';
import 'package:teledentistry/src/resources/colors.dart';
import 'package:teledentistry/src/resources/image_assets.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/ui_screens/app_drawer/app_drawer.dart';
import 'package:teledentistry/src/utils/app_navigator/app_navigator.dart';
import 'package:teledentistry/src/utils/common_widgets/common_widget.dart';
import 'package:teledentistry/src/utils/firebase_ops/fcm.dart';
import 'package:teledentistry/src/utils/sharedpreference_helper/sharepreference_helper.dart';

class Dashboard extends StatefulWidget {
  static const String routeName = "/Dashboard";
  final String roomName;
  Dashboard({Key key, this.roomName}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> with WidgetsBindingObserver {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var exitApp;

  MainBloc mainBloc;

  String userType, name, email;

  @override
  void initState() {
    mainBloc = BlocProvider.of<MainBloc>(context);
    exitApp = 0;
    WidgetsBinding.instance.addObserver(this);
    callData();
    super.initState();
  }

  callData() async {
    await Fcm.getInstance().chatNotification(context: context);
    userType = await SharePreferencesHelper.getInstant()
        .getString(SharePreferencesHelper.User_role);
    name = await SharePreferencesHelper.getInstant()
        .getString(SharePreferencesHelper.Full_Name);
    email = await SharePreferencesHelper.getInstant()
        .getString(SharePreferencesHelper.Email);
    // getcallCheck();
    if (mounted) setState(() {});
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.inactive:
        print("inactive");
        break;
      case AppLifecycleState.paused:
        print("paused");
        break;
      case AppLifecycleState.resumed:
        getcallCheck();
        print("resumed");
        break;
      case AppLifecycleState.detached:
        print("detached");
        break;
    }
  }

  getcallCheck() async {
    if (mainBloc.isCallTaken) {
    } else {
      GetRoomsData result = await mainBloc.callGetRoomsApi(context: context);
      if (result.roomName != "" && mounted) {
        mainBloc.isCallTaken = true;
        AppNavigator.launchPatientsListScreen(context, result.roomName);
      }
    }
  }

  Future<bool> onWillPop() {
    Fluttertoast.showToast(
        msg: StringHelper.press_back_again_to_exit,
        backgroundColor: ColorsHelper.blueColor(),
        textColor: Colors.white);
    exitApp++;
    Future.delayed(Duration(milliseconds: 1000), () {
      if (exitApp >= 2) {
        exit(0);
      } else {
        exitApp = 0;
      }
    });
    return Future.value(false);
  }

  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
      child: Container(
        child: Scaffold(
          key: _scaffoldKey,
          drawer: LeftDrawer(
            pageId: StringHelper.dashboard,
          ),
          backgroundColor: ColorsHelper.whiteColor(),
          appBar: appBarBody(
              title: StringHelper.dashboard,
              textColor: ColorsHelper.greyColor(),
              fontWeight: FontWeight.w600,
              action1Icon: ImageAssets.menu_button_5,
              onTapaction1: () {
                _scaffoldKey.currentState.openDrawer();
              }),
          body: dashboardBody(),
          bottomNavigationBar: bottonNavBar(context: context),
        ),
      ),
    );
  }

  Widget dashboardBody() => Container(
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 50),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          textHelper(
              textAlign: TextAlign.left,
              title: "Welcome ${userType == "2" ? "Doctor" : "Patient"}, $name",
              fontSize: 17,
              fontWeight: FontWeight.w600),
          //vector
          Container(
            height: MediaQuery.of(context).size.height * 0.2,
            child: assetImageHelper(
              image: ImageAssets.vector,
            ),
          ),
          //start_a_Call
          submitButton(
              onPressed: () {
                AppNavigator.launchPatientsListScreen(context, null);
              },
              title: StringHelper.start_a_Call),
        ],
      ));
}
