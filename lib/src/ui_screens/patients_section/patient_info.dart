import 'package:flutter/material.dart';
import 'package:teledentistry/src/models/SearchListModel.dart';
import 'package:teledentistry/src/resources/colors.dart';
import 'package:teledentistry/src/resources/image_assets.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/utils/app_navigator/app_navigator.dart';
import 'package:teledentistry/src/utils/buttons/button.dart';
import 'package:teledentistry/src/utils/common_widgets/common_widget.dart';
import 'package:teledentistry/src/utils/sharedpreference_helper/sharepreference_helper.dart';

class PatientsInfo extends StatefulWidget {
  static const String routeName = "/PatientsInfo";
  final Record record;

  const PatientsInfo({Key key, this.record}) : super(key: key);
  @override
  _PatientsInfoState createState() => _PatientsInfoState();
}

class _PatientsInfoState extends State<PatientsInfo> {
  String userType, name, email;
  @override
  void initState() {
    callData();
    super.initState();
  }

  callData() async {
    userType = await SharePreferencesHelper.getInstant()
        .getString(SharePreferencesHelper.User_role);
    name = await SharePreferencesHelper.getInstant()
        .getString(SharePreferencesHelper.User_role);
    email = await SharePreferencesHelper.getInstant()
        .getString(SharePreferencesHelper.Email);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.5,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                  ColorsHelper.blueColor(),
                  ColorsHelper.lightBlueColor()
                ])),
          ),
          Scaffold(
            backgroundColor: Colors.transparent,
            appBar: appBarBody(
              title: StringHelper.patient_info,
              textColor: ColorsHelper.whiteColor(),
              fontWeight: FontWeight.w600,
              isLeading: true,
              onTapLeading: () => AppNavigator.popBackStack(context),
              leadingColor: ColorsHelper.whiteColor(),
              // action1Icon: ImageAssets.menu_button_4,
              // onTapaction1: () {}
            ),
            body: patientsInfoBody(),
            bottomNavigationBar: bottonNavBar(context: context),
          )
        ],
      ),
    );
  }

  Widget patientsInfoBody() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: MediaQuery.of(context).size.height * 0.07),
          Container(
            padding: EdgeInsets.only(left: 20),
            child: textHelper(
                textAlign: TextAlign.left,
                title: widget.record.fullName,
                fontSize: 25,
                fontWeight: FontWeight.w600,
                textColor: ColorsHelper.whiteColor()),
          ),
          SizedBox(height: 5),
          Container(
            padding: EdgeInsets.only(left: 20),
            child: textHelper(
                fontSize: 20,
                title: widget.record.userPhone,
                textColor: ColorsHelper.whiteColor()),
          ),
          SizedBox(height: 30),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: ColorsHelper.whiteColor(),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: SingleChildScrollView(child: linksBody()),
            ),
          ),
        ],
      );

  Widget linksBody() => Column(
        children: [
          buttonHelper(
              onTap: () => AppNavigator.launchVideoCallingScreen(
                  context, widget.record, null),
              title: StringHelper.text_Consent_Form,
              img: ImageAssets.message_square),
          SizedBox(height: 30),
          buttonHelper(
              onTap: () => AppNavigator.launchVideoCallingScreen(
                  context, widget.record, null),
              title: StringHelper.email_Consent_Form,
              img: ImageAssets.at_rate),
          SizedBox(height: 30),
          buttonHelper(
              onTap: () => AppNavigator.launchVideoCallingScreen(
                  context, widget.record, null),
              title: StringHelper.link_Existing_Consent_Form,
              img: ImageAssets.vector_),
        ],
      );

  Widget buttonHelper({String title, Function onTap, String img}) =>
      ButtonColor(
          color: ColorsHelper.lightBlueColor(),
          radius: 10,
          onPressed: onTap,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  title,
                  style: TextStyle(
                      color: ColorsHelper.whiteColor(),
                      fontSize: 13,
                      fontWeight: FontWeight.w600),
                ),
                assetImageHelper(image: img, height: 20)
              ],
            ),
          ));
}
