import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jitsi_meet/feature_flag/feature_flag_enum.dart';
import 'package:jitsi_meet/jitsi_meet.dart';
import 'package:jitsi_meet/jitsi_meeting_listener.dart';
import 'package:teledentistry/src/bloc/bloc_main/main_bloc.dart';
import 'package:teledentistry/src/models/SearchListModel.dart';
import 'package:teledentistry/src/resources/colors.dart';
import 'package:teledentistry/src/resources/image_assets.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/utils/app_navigator/app_navigator.dart';
import 'package:teledentistry/src/utils/common_widgets/common_widget.dart';
import 'package:teledentistry/src/utils/sharedpreference_helper/sharepreference_helper.dart';

class VideoCalling extends StatefulWidget {
  static const String routeName = "/VideoCalling";
  final Record record;
  final String roomName;

  const VideoCalling({Key key, this.record, this.roomName}) : super(key: key);
  @override
  _VideoCallingState createState() => _VideoCallingState();
}

class _VideoCallingState extends State<VideoCalling> {
  bool isCallstart = true;

  final serverText = TextEditingController();
  MainBloc mainBloc;
  DateTime startTime, endTime;
  String callerId, receiverId, roomName;

  @override
  void initState() {
    mainBloc = BlocProvider.of<MainBloc>(context);
    super.initState();
    JitsiMeet.addListener(JitsiMeetingListener(
        onConferenceWillJoin: _onConferenceWillJoin,
        onConferenceJoined: _onConferenceJoined,
        onConferenceTerminated: _onConferenceTerminated,
        onError: _onError));
     //callData();
    Future.delayed(Duration(seconds: 1)).then((value) => _joinMeeting());
  }

  callData() async {
    callerId = await SharePreferencesHelper.getInstant()
        .getString(SharePreferencesHelper.Id);
    if (widget.roomName != null) {
      roomName = widget.roomName;
      _joinMeeting();
    } else {
      roomName = callerId + "_" + widget.record.id;
    }
    if (mounted) setState(() {});
  }

  sendNotifications() async {
    var result = await mainBloc.callSendNotificationsApi(
      receiverId: widget.record.id,
      roomName: roomName,
      context: context,
    );
    if (result != null) {
      _joinMeeting();
    }
  }

  endCall() async {
    if (mounted)
      setState(() {
        isCallstart = false;
      });
    if (widget.roomName == null) {
      await mainBloc.callSendCallLogsApi(
          callerUserId: callerId,
          receiverUserId: widget.record.id,
          dtOfCall: DateTime.now().millisecondsSinceEpoch.toString(),
          timeOfCall: startTime.millisecondsSinceEpoch.toString(),
          endOfCall: endTime.millisecondsSinceEpoch.toString(),
          context: context);
    }
    var removeRoomCaller = await mainBloc.callRemoveRoomApi(context: context);
    var removeRoomReceiver = await mainBloc.callRemoveRoomApi(
        receiverId: roomName.split('_').last, context: context);
    if (removeRoomCaller != null && removeRoomReceiver != null) {
      if (widget.roomName != null) {
        mainBloc.isCallTaken = false;
        AppNavigator.launchDashboardScreen(context, null);
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.5,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                  ColorsHelper.blueColor(),
                  ColorsHelper.lightBlueColor()
                ])),
          ),
          Scaffold(
            backgroundColor: Colors.transparent,
            appBar: appBarBody(
              title: StringHelper.patient_info,
              textColor: ColorsHelper.whiteColor(),
              fontWeight: FontWeight.w600,
              isLeading: true,
              onTapLeading: () => AppNavigator.popBackStack(context),
              leadingColor: ColorsHelper.whiteColor(),
              // action1Icon: ImageAssets.menu_button_4,
              // onTapaction1: () {}
            ),
            body: videoCallingBody(),
            bottomNavigationBar: bottonNavBar(context: context),
          )
        ],
      ),
    );
  }

  Widget videoCallingBody() => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: MediaQuery.of(context).size.height * 0.07),
          Container(
            padding: EdgeInsets.only(left: 20),
            child: textHelper(
                textAlign: TextAlign.left,
                title: widget.record != null ? widget.record.fullName : " ",
                fontSize: 25,
                fontWeight: FontWeight.w600,
                textColor: ColorsHelper.whiteColor()),
          ),
          SizedBox(height: 5),
          Container(
            padding: EdgeInsets.only(left: 20),
            child: textHelper(
                fontSize: 20,
                title: widget.record != null ? widget.record.userPhone : " ",
                textColor: ColorsHelper.whiteColor()),
          ),
          SizedBox(height: 30),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: ColorsHelper.whiteColor(),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: SingleChildScrollView(child: callingBody()),
            ),
          ),
        ],
      );

  Widget callingBody() => Column(
        children: [
          //consent_Form_Signed
          if (isCallstart)
            Container(
              padding: EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: textHelper(
                        textColor: ColorsHelper.greyColor(),
                        fontSize: 15,
                        title: StringHelper.consent_Form_Signed),
                  ),
                  assetImageHelper(image: ImageAssets.group_35),
                ],
              ),
            ),
          //line
          if (isCallstart) line(margin: 10),
          SizedBox(height: 50),
//Calling functionm //facetime
          if (isCallstart)
            Stack(
              children: [
                assetImageHelper(
                  image: ImageAssets.faceTime,
                  height: 100,
                  width: 100,
                ),
                Material(
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.transparent,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(100),
                    onTap: () {
                      if (mounted) {
                        sendNotifications();
                      }
                    },
                    child: Container(
                      height: 100,
                      width: 100,
                      color: Colors.transparent,
                    ),
                  ),
                )
              ],
            ),
          if (isCallstart) SizedBox(height: 10),
          if (isCallstart)
            //start_a_Call
            textHelper(
                textColor: ColorsHelper.greyColor(),
                fontSize: 15,
                fontWeight: FontWeight.w500,
                title: StringHelper.start_a_Call),
          //done calling
          if (!isCallstart)
            //your_appointment_has_ended
            textHelper(
                textColor: ColorsHelper.greyColor(),
                fontSize: 15,
                title: StringHelper.your_appointment_has_ended),
          if (!isCallstart) SizedBox(height: 30),
          //group_52
          if (!isCallstart)
            Stack(
              children: [
                assetImageHelper(
                  image: ImageAssets.group_52,
                  height: 100,
                  width: 100,
                ),
                Material(
                  borderRadius: BorderRadius.circular(100),
                  color: Colors.transparent,
                  child: InkWell(
                    borderRadius: BorderRadius.circular(100),
                    onTap: () {
                      AppNavigator.popBackStack(context);
                    },
                    child: Container(
                      height: 100,
                      width: 100,
                      color: Colors.transparent,
                    ),
                  ),
                )
              ],
            ),
        ],
      );

  //Jitsi code
  _joinMeeting() async {
    String serverUrl =
        serverText.text?.trim()?.isEmpty ?? "" ? null : serverText.text;
    try {
      Map<FeatureFlagEnum, bool> featureFlags = {
        FeatureFlagEnum.WELCOME_PAGE_ENABLED: false,
      };
      if (Platform.isAndroid) {
        featureFlags[FeatureFlagEnum.CALL_INTEGRATION_ENABLED] = false;
      } else if (Platform.isIOS) {
        featureFlags[FeatureFlagEnum.PIP_ENABLED] = false;
      }

      var options = JitsiMeetingOptions()
        ..room = roomName ?? "TeleDentistry_ricky"
        ..serverURL = serverUrl
        ..subject = "TeleDentistry"
        ..userDisplayName = "TeleDentistry User"
        ..userEmail = "TeleDentistry@gmail.com"
       
        ..audioOnly = false
        ..audioMuted = false
        ..videoMuted = false
        ..featureFlags.addAll(featureFlags);
      await JitsiMeet.joinMeeting(
        options,
        listener: JitsiMeetingListener(
            onConferenceWillJoin: ({message}) {},
            onConferenceJoined: ({message}) {
              startTime = DateTime.now();
            },
            onConferenceTerminated: ({message}) {
              endTime = DateTime.now();
              endCall();
            }),
      );
    } catch (error) {}
  }

  void _onConferenceWillJoin({message}) {
    debugPrint("_onConferenceWillJoin broadcasted with message: $message");
  }

  void _onConferenceJoined({message}) {
    debugPrint("_onConferenceJoined broadcasted with message: $message");
  }

  void _onConferenceTerminated({message}) {
    debugPrint("_onConferenceTerminated broadcasted with message: $message");
  }

  _onError(error) {
    debugPrint("_onError broadcasted: $error");
  }
}
