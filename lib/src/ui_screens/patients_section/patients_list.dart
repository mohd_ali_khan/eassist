import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:jitsi_meet/feature_flag/feature_flag_enum.dart';
import 'package:jitsi_meet/jitsi_meet.dart';
import 'package:jitsi_meet/jitsi_meeting_listener.dart';
import 'package:teledentistry/src/bloc/bloc_main/main_bloc.dart';
import 'package:teledentistry/src/data/network/api.dart';
import 'package:teledentistry/src/data/network/api_response.dart';
import 'package:teledentistry/src/data/network/rest_client_new.dart';
import 'package:teledentistry/src/models/SearchListModel.dart';
import 'package:teledentistry/src/resources/colors.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/utils/app_navigator/app_navigator.dart';
import 'package:teledentistry/src/utils/buttons/button.dart';
import 'package:teledentistry/src/utils/common_widgets/common_widget.dart';
import 'package:teledentistry/src/utils/listview_scroll_behavior/listview_scroll_behavior.dart';
import 'package:teledentistry/src/utils/sharedpreference_helper/sharepreference_helper.dart';
import 'package:teledentistry/src/utils/validator/validator.dart';

class PatientsList extends StatefulWidget {
  static const String routeName = "/PatientsList";
  final String roomName;

  const PatientsList({Key key, this.roomName}) : super(key: key);
  @override
  _PatientsListState createState() => _PatientsListState();
}

class _PatientsListState extends State<PatientsList> {
  TextEditingController searchController = TextEditingController();
  FocusNode searchNode = FocusNode();

  //sersrch
  ScrollController _scrollController = new ScrollController();
  bool isLoading = false;
  SearchListData searchData = SearchListData();
  List<Record> recordList = List<Record>();
  final dio = new Dio();
  int count = 0;

  String userType, userList;

  //video calling code
  bool isCallstart = true;

  final serverText = TextEditingController();
  MainBloc mainBloc;
  DateTime startTime, endTime;
  String callerId, receiverId, roomName;

  @override
  void initState() {
    mainBloc = BlocProvider.of<MainBloc>(context);
    this._getMoreData();
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _getMoreData();
      }
    });
    JitsiMeet.addListener(JitsiMeetingListener(
        onConferenceWillJoin: _onConferenceWillJoin,
        onConferenceJoined: _onConferenceJoined,
        onConferenceTerminated: _onConferenceTerminated,
        onError: _onError));
    callData();
  }

  void _getMoreData() async {
    if (!isLoading && mounted) {
      setState(() {
        isLoading = true;
      });
      var token = await SharePreferencesHelper.getInstant()
          .getAccessToken(SharePreferencesHelper.Access_Token);
      var userType = await SharePreferencesHelper.getInstant()
          .getString(SharePreferencesHelper.User_role);
      if (userType == "2") {
        userType = "3";
      } else if (userType == "3") {
        userType = "2";
      }
      var body = {
        "type": userType,
        "search_text": searchController.text.trim(),
        "page_no": count
      };
      List<Record> tempList = new List<Record>();
      ApiResponse apiResponse = await restClientNew.post(context, Api.search,
          body: body, header: {'version': '1.0.0', 'Authorization': token});
      if (apiResponse.status == 1) {
        searchData = SearchListData.fromJson(apiResponse.data[0]);
        count++;
        for (int i = 0; i < searchData.record.length; i++) {
          tempList.add(searchData.record[i]);
        }
      }
      setState(() {
        isLoading = false;
        recordList.addAll(tempList);
      });
    }
  }

  //video calling code
  callData() async {
    userList = await SharePreferencesHelper.getInstant()
        .getString(SharePreferencesHelper.User_role);
    callerId = await SharePreferencesHelper.getInstant()
        .getString(SharePreferencesHelper.Id);
    if (widget.roomName != null) {
      roomName = widget.roomName;
      _joinMeeting(null);
    }
    if (mounted) setState(() {});
  }

  sendNotifications(Record record) async {
    var result = await mainBloc.callSendNotificationsApi(
      receiverId: record.id,
      roomName: callerId + "_" + record.id,
      context: context,
    );
    if (result != null) {
      _joinMeeting(record);
    }
  }

  endCall(Record record) async {
    if (mounted)
      setState(() {
        isCallstart = false;
      });
    if (widget.roomName == null) {
      await mainBloc.callSendCallLogsApi(
          callerUserId: callerId,
          receiverUserId: record.id,
          dtOfCall: DateTime.now().millisecondsSinceEpoch.toString(),
          timeOfCall: startTime.millisecondsSinceEpoch.toString(),
          endOfCall: endTime.millisecondsSinceEpoch.toString(),
          context: context);
    }
    var removeRoomCaller = await mainBloc.callRemoveRoomApi(context: context);
    var removeRoomReceiver = await mainBloc.callRemoveRoomApi(
        receiverId: widget.roomName != null
            ? widget.roomName.split('_').last
            : record.id,
        context: context);
    if (removeRoomCaller != null && removeRoomReceiver != null) {
      if (widget.roomName != null) {
        mainBloc.isCallTaken = false;
        AppNavigator.launchDashboardScreen(context, null);
      }
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    recordList.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.5,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                  ColorsHelper.blueColor(),
                  ColorsHelper.lightBlueColor()
                ])),
          ),
          Scaffold(
            backgroundColor: Colors.transparent,
            appBar: appBarBody(
              title: userList == "2"
                  ? StringHelper.patient_Search
                  : StringHelper.doctor_Search,
              textColor: ColorsHelper.whiteColor(),
              fontWeight: FontWeight.w600,
              isLeading: true,
              onTapLeading: () => AppNavigator.popBackStack(context),
              leadingColor: ColorsHelper.whiteColor(),
            ),
            body: patientsListBody(),
            bottomNavigationBar: bottonNavBar(context: context),
          )
        ],
      ),
    );
  }

  Widget patientsListBody() => Column(
        children: [
          SizedBox(height: MediaQuery.of(context).size.height * 0.1),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: ColorsHelper.whiteColor(),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: patientsSearchBody(),
            ),
          ),
        ],
      );

  Widget patientsSearchBody() => Column(
        children: [
          // //Search , filter
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //   children: [
          //     //search
          //     textHelper(
          //         fontSize: 13,
          //         textColor: ColorsHelper.greyColor(),
          //         textAlign: TextAlign.end,
          //         title: StringHelper.search),
          //     InkWell(
          //       borderRadius: BorderRadius.circular(50),
          //       onTap: () {},
          //       child: Container(
          //           width: 35,
          //           padding: EdgeInsets.all(5),
          //           child: Image.asset(
          //             ImageAssets.filter_button,
          //           )),
          //     )
          //   ],
          // ),
          searchForm(),
          Flexible(
            child: ScrollConfiguration(
              behavior: ListViewScrollBehavior(),
              child: ListView.builder(
                  shrinkWrap: true,
                  controller: _scrollController,
                  itemCount: recordList.length + 1,
                  //specialistObject.doctorList.length,
                  itemBuilder: (context, index) {
                    if (index == recordList.length) {
                      return _buildProgressIndicator();
                    } else {
                      return listBox(record: recordList[index]);
                    }
                  }),
            ),
          )
        ],
      );

  Widget searchForm() => Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          //email_ID
          formTextFeild(
              context: context,
              controller: searchController,
              focusNode: searchNode,
              nextFocusNode: null,
              hintText: StringHelper.search,
              textInputType: TextInputType.text,
              textInputAction: TextInputAction.done,
              validation: CommonValidator.emptyValidation),
          SizedBox(height: 10),
          //search button
          Container(
              child: ButtonColor(
                  height: 35,
                  width: 100,
                  color: ColorsHelper.lightBlueColor(),
                  radius: 10,
                  onPressed: () {
                    count = 0;
                    recordList = [];
                    callUnfocus(context: context);
                    _getMoreData();
                  },
                  child: Text(
                    StringHelper.search,
                    style: TextStyle(
                        color: ColorsHelper.whiteColor(),
                        fontSize: 13,
                        fontWeight: FontWeight.w600),
                  ))),
          SizedBox(height: 10),
        ],
      );

  Widget _buildProgressIndicator() {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Center(
        child: new Opacity(
          opacity: isLoading ? 0.0 : 00,
          child: new CircularProgressIndicator(),
        ),
      ),
    );
  }

  Widget listBox({Record record}) => Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: ColorsHelper.greyTextBoxColor(),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          //user info
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                textHelper(
                    textAlign: TextAlign.left,
                    title: record.fullName,
                    fontSize: 17,
                    fontWeight: FontWeight.w600),
                SizedBox(height: 5),
                textHelper(title: record.userPhone),
              ],
            ),
          ),
          //select button
          Container(
              child: ButtonColor(
                  height: 35,
                  width: 100,
                  color: ColorsHelper.lightBlueColor(),
                  radius: 10,
                  onPressed: () {
                    callUnfocus(context: context);
                    sendNotifications(record);
                  },
                  // AppNavigator.launchPatientsInfoScreen(context, record),
                  child: Text(
                    StringHelper.select,
                    style: TextStyle(
                        color: ColorsHelper.whiteColor(),
                        fontSize: 13,
                        fontWeight: FontWeight.w600),
                  )))
        ],
      ));

  //Jitsi code
  _joinMeeting(Record record) async {
    String serverUrl =
        serverText.text?.trim()?.isEmpty ?? "" ? null : serverText.text;
    try {
      Map<FeatureFlagEnum, bool> featureFlags = {
        FeatureFlagEnum.WELCOME_PAGE_ENABLED: false,
      };
      if (Platform.isAndroid) {
        featureFlags[FeatureFlagEnum.CALL_INTEGRATION_ENABLED] = false;
      } else if (Platform.isIOS) {
        featureFlags[FeatureFlagEnum.PIP_ENABLED] = false;
      }

      var options = JitsiMeetingOptions()
        ..room = roomName ?? "TeleDentistry_ricky"
        ..serverURL = serverUrl
        ..subject = "TeleDentistry"
        ..userDisplayName = "TeleDentistry User"
        ..userEmail = "TeleDentistry@gmail.com"
        
        ..audioOnly = false
        ..audioMuted = false
        ..videoMuted = false
        ..featureFlags.addAll(featureFlags);
      await JitsiMeet.joinMeeting(
        options,
        listener: JitsiMeetingListener(
            onConferenceWillJoin: ({message}) {},
            onConferenceJoined: ({message}) {
              startTime = DateTime.now();
            },
            onConferenceTerminated: ({message}) {
              endTime = DateTime.now();
              endCall(record);
            }),
      );
    } catch (error) {}
  }

  void _onConferenceWillJoin({message}) {
    debugPrint("_onConferenceWillJoin broadcasted with message: $message");
  }

  void _onConferenceJoined({message}) {
    debugPrint("_onConferenceJoined broadcasted with message: $message");
  }

  void _onConferenceTerminated({message}) {
    debugPrint("_onConferenceTerminated broadcasted with message: $message");
  }

  _onError(error) {
    debugPrint("_onError broadcasted: $error");
  }
}
