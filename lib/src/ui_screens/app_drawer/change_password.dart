import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teledentistry/src/bloc/bloc_login/login_bloc.dart';
import 'package:teledentistry/src/bloc/bloc_main/main_bloc.dart';
import 'package:teledentistry/src/resources/colors.dart';
import 'package:teledentistry/src/resources/image_assets.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/utils/app_navigator/app_navigator.dart';
import 'package:teledentistry/src/utils/common_widgets/common_widget.dart';
import 'package:teledentistry/src/utils/popup_dialogs/popup_dialogs.dart';
import 'package:teledentistry/src/utils/validator/validator.dart';

class ChangePassword extends StatefulWidget {
  static const String routeName = "/ChangePassword";
  ChangePassword({Key key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<ChangePassword> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isHide1 = true, isHide2 = true;

  TextEditingController passswordController = TextEditingController();
  TextEditingController confirmPwdController = TextEditingController();

  FocusNode passwordNode = FocusNode();
  FocusNode confirmPwdNode = FocusNode();

  MainBloc mainBloc;
  LoginBloc loginBloc = LoginBloc();

  bool isRequested = false;

  @override
  void initState() {
    mainBloc = BlocProvider.of<MainBloc>(context);
    super.initState();
  }

  callData() {
    mainBloc.tp();
  }

  submit() async {
    var result = await loginBloc.callChangePasswordApi(
        oldPassword: passswordController.text.trim(),
        newPassword: confirmPwdController.text.trim(),
        context: context);
    if (result != null) {
      if (mounted) {
        // SharePreferencesHelper.getInstant().clearPreference();
        // AppNavigator.launchSplashScreen(context);
        AppNavigator.popBackStack(context);
        PopupDialogs.displayOnlyMessage(context, result);
      }
    }
  }

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callUnfocus(context: context),
      child: Scaffold(
        backgroundColor: ColorsHelper.whiteColor(),
        appBar: appBarBody(
          title: StringHelper.change_Password,
          textColor: ColorsHelper.greyColor(),
          fontWeight: FontWeight.w600,
          isLeading: true,
          onTapLeading: () => AppNavigator.popBackStack(context),
          // action1Icon: ImageAssets.menu_button_5,
          // onTapaction1: () {}
        ),
        body: SingleChildScrollView(child: loginBody()),
        bottomNavigationBar: bottonNavBar(context: context),
      ),
    );
  }

  Widget loginBody() => Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Column(
        children: [
          //vector
          Container(
            height: MediaQuery.of(context).size.height * 0.2,
            child: assetImageHelper(
              image: ImageAssets.vector,
            ),
          ),
          SizedBox(height: 20),

          userForm(),
          SizedBox(height: 10),
          //buttons
          buttons(),
          SizedBox(height: 10),
        ],
      ));

  Widget userForm() => Form(
      key: _formKey,
      // autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          //passswordController
          formTextFeild(
              context: context,
              controller: passswordController,
              focusNode: passwordNode,
              nextFocusNode: confirmPwdNode,
              hintText: StringHelper.password,
              textInputType: TextInputType.text,
              textInputAction: TextInputAction.next,
              validation: CommonValidator.passValidation,
              obscureText: isHide1,
              suffixIcon: true,
              suffixFuncton: () {
                if (mounted) {
                  isHide1 = !isHide1;
                  setState(() {});
                }
              }),
          SizedBox(height: 10),
          formTextFeild(
              context: context,
              controller: confirmPwdController,
              focusNode: confirmPwdNode,
              nextFocusNode: null,
              hintText: StringHelper.new_password,
              textInputType: TextInputType.text,
              textInputAction: TextInputAction.done,
              validation: CommonValidator.passValidation,
              obscureText: isHide2,
              suffixIcon: true,
              suffixFuncton: () {
                if (mounted) {
                  isHide2 = !isHide2;
                  setState(() {});
                }
              }),
          SizedBox(height: 10),
        ],
      ));

  Widget buttons() => Row(
        children: [
          //reset_Password
          Expanded(
              child: submitButton(
                  onPressed: () {
                    callUnfocus(context: context);
                    loginBloc.onLoginClick(_formKey).then((value) {
                      if (value) {
                        submit();
                      } else {
                        if (mounted) {
                          setState(() {});
                        }
                      }
                    });
                  },
                  title: isRequested
                      ? StringHelper.send_again
                      : StringHelper.reset_Password)),
        ],
      );
}
