import 'package:flutter/material.dart';
import 'package:teledentistry/src/bloc/bloc_login/login_bloc.dart';
import 'package:teledentistry/src/data/network/api.dart';
import 'package:teledentistry/src/models/UrlLinksModel.dart';
import 'package:teledentistry/src/resources/colors.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/utils/app_navigator/app_navigator.dart';
import 'package:teledentistry/src/utils/common_widgets/common_widget.dart';
import 'package:url_launcher/url_launcher.dart';

class Settings extends StatefulWidget {
  static const String routeName = "/Settings";
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  UrlLinksData urlData;
  LoginBloc loginBloc = LoginBloc();
  @override
  void initState() {
    super.initState();
  }

  openWebPage({String title}) async {
    // urlData = await loginBloc.callUrlLinksApi(context: context, apiLink: title);
    // if (urlData != null && mounted) setState(() {});
    if (await canLaunch(title)) {
      await launch(
        title,
      );
    } else {
      throw 'Could not launch ${urlData.url}';
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.5,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                  ColorsHelper.blueColor(),
                  ColorsHelper.lightBlueColor()
                ])),
          ),
          Scaffold(
            backgroundColor: Colors.transparent,
            appBar: appBarBody(
              title: StringHelper.settings,
              textColor: ColorsHelper.whiteColor(),
              fontWeight: FontWeight.w600,
              isLeading: true,
              onTapLeading: () => AppNavigator.popBackStack(context),
              leadingColor: ColorsHelper.whiteColor(),
              // action1Icon: ImageAssets.menu_button_4,
              // onTapaction1: () {}
            ),
            body: consentFormBody(),
            bottomNavigationBar: bottonNavBar(context: context),
          )
        ],
      ),
    );
  }

  Widget consentFormBody() => Column(
        children: [
          SizedBox(height: MediaQuery.of(context).size.height * 0.1),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: ColorsHelper.whiteColor(),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: SingleChildScrollView(child: textBody()),
            ),
          ),
        ],
      );

  Widget textBody() => Column(
        children: [
          textRowHelper(
              onTap: () {
                AppNavigator.launchChangePasswordScreen(context);
              },
              title: StringHelper.change_Password),
          line(),
          textRowHelper(
              onTap: () {
                openWebPage(title: "https://dentalbilling.com/terms-of-use/");
              },
              title: "Terms of Use"),
          line(),
          textRowHelper(
              onTap: () {
                openWebPage(title: "https://dentalbilling.com/privacy-policy/");
              },
              title: StringHelper.privacy_Policy),
        ],
      );

  Widget textRowHelper({String title, Function onTap}) => Material(
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(5),
        child: InkWell(
            borderRadius: BorderRadius.circular(5),
            onTap: onTap,
            child: Container(
              color: Colors.transparent,
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  textHelper(title: title),
                  Icon(Icons.arrow_forward_ios)
                ],
              ),
            )),
      );
}
