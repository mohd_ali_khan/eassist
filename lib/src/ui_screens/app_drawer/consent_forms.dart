import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teledentistry/src/bloc/bloc_login/login_bloc.dart';
import 'package:teledentistry/src/bloc/bloc_main/main_bloc.dart';
import 'package:teledentistry/src/data/network/api.dart';
import 'package:teledentistry/src/models/UrlLinksModel.dart';
import 'package:teledentistry/src/resources/colors.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/utils/app_navigator/app_navigator.dart';
import 'package:teledentistry/src/utils/common_widgets/common_widget.dart';

class ConsentForm extends StatefulWidget {
  static const String routeName = "/ConsentForm";
  @override
  _ConsentFormState createState() => _ConsentFormState();
}

class _ConsentFormState extends State<ConsentForm> {
  UrlLinksData urlData;
  LoginBloc loginBloc = LoginBloc();
  MainBloc mainBloc;
  @override
  void initState() {
    callData();
    super.initState();
  }

  callData() async {
    mainBloc = BlocProvider.of<MainBloc>(context);
    mainBloc.tp();
    urlData = await loginBloc.callUrlLinksApi(
        context: context, apiLink: Api.consent_form);
    if (urlData != null && mounted) setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.5,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                  ColorsHelper.blueColor(),
                  ColorsHelper.lightBlueColor()
                ])),
          ),
          Scaffold(
            backgroundColor: Colors.transparent,
            appBar: appBarBody(
              title: "Terms and Conditions",
              textColor: ColorsHelper.whiteColor(),
              fontWeight: FontWeight.w600,
              isLeading: true,
              onTapLeading: () => AppNavigator.popBackStack(context),
              leadingColor: ColorsHelper.whiteColor(),
              // action1Icon: ImageAssets.menu_button_4,
              // onTapaction1: () {}
            ),
            body: consentFormBody(),
            bottomNavigationBar: bottonNavBar(context: context),
          )
        ],
      ),
    );
  }

  Widget consentFormBody() => Column(
        children: [
          SizedBox(height: MediaQuery.of(context).size.height * 0.1),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: ColorsHelper.whiteColor(),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: SingleChildScrollView(child: textBody()),
            ),
          ),
        ],
      );

  Widget textBody() => Column(
        children: [
          if (urlData != null)
            textHelper(textAlign: TextAlign.left, title: urlData.doctorUrl),
          // SizedBox(height: 50),
          // if (mainBloc.userType == '3' && urlData != null)
          //   textHelper(title: urlData.patientUrl),
          SizedBox(height: 10),
          Container(
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: submitButton(
                  onPressed: () {
                    if (mainBloc.userType == '3' && urlData != null) {
                      AppNavigator.launchConsentFormPatientScreen(context);
                    } else {
                      Navigator.of(context).pop(true);
                    }
                  },
                  title: StringHelper.i_agree)),
          SizedBox(height: 10),
        ],
      );
}
