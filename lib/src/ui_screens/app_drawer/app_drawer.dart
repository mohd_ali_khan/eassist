import 'package:teledentistry/src/resources/colors.dart';
import 'package:teledentistry/src/resources/image_assets.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/utils/app_navigator/app_navigator.dart';
import 'package:teledentistry/src/utils/common_widgets/common_widget.dart';
import 'package:teledentistry/src/utils/listview_scroll_behavior/listview_scroll_behavior.dart';
import 'package:flutter/material.dart';
import 'package:teledentistry/src/utils/sharedpreference_helper/sharepreference_helper.dart';

class LeftDrawer extends StatefulWidget {
  final String pageId;
  LeftDrawer({Key key, @required this.pageId}) : super(key: key);

  @override
  _LeftDrawerState createState() => _LeftDrawerState();
}

class _LeftDrawerState extends State<LeftDrawer> {
  String userName, profilePic;

  @override
  void initState() {
    super.initState();
    getData();
  }

  void getData() async {
    userName = "Ramanpreet Singh Raina";
  }

  List<DrawerDetails> arrListDetail = [
    //start_a_Call
    DrawerDetails(
        drawerElementId: StringHelper.start_a_Call,
        titleText: StringHelper.start_a_Call,
        imageCustom: ImageAssets.appD1,
        isTrailing: false),
    //call_Log
    DrawerDetails(
        drawerElementId: StringHelper.call_Log,
        titleText: StringHelper.call_Log,
        imageCustom: ImageAssets.appD2,
        isTrailing: false),
    //consent_Forms
    DrawerDetails(
        drawerElementId: StringHelper.consent_Forms,
        titleText: StringHelper.consent_Forms,
        imageCustom: ImageAssets.appD3,
        isTrailing: false),
    //account
    DrawerDetails(
        drawerElementId: StringHelper.account,
        titleText: StringHelper.account,
        imageCustom: ImageAssets.appD4,
        isTrailing: false),
    //settings
    DrawerDetails(
        drawerElementId: StringHelper.settings,
        titleText: StringHelper.settings,
        imageCustom: ImageAssets.appD5,
        isTrailing: false),
    // //contact
    // DrawerDetails(
    //     drawerElementId: StringHelper.contact,
    //     titleText: StringHelper.contact,
    //     imageCustom: ImageAssets.appD6,
    //     isTrailing: false),
    //logout
    DrawerDetails(
        drawerElementId: StringHelper.logout,
        titleText: StringHelper.logout,
        imageCustom: ImageAssets.appD7,
        isTrailing: false),
  ];

  gotToScreen(String titleText, BuildContext context) {
    AppNavigator.popBackStack(context);
    switch (titleText) {
      // start_a_Call
      case StringHelper.start_a_Call:
        AppNavigator.launchPatientsListScreen(context, null);
        break;
      // call_Log
      case StringHelper.call_Log:
        AppNavigator.launchCallLogsScreen(context);
        break;
      // consent_Forms
      case StringHelper.consent_Forms:
        AppNavigator.launchConsentFormScreen(context);
        break;
      // account
      case StringHelper.account:
        AppNavigator.launchAccountScreen(context);
        break;
      // settings
      case StringHelper.settings:
        AppNavigator.launchSettingsScreen(context);
        break;
      // contact
      case StringHelper.contact:
        //  AppNavigator.launchContactUsScreen(context);
        break;
      // logout
      case StringHelper.logout:
        SharePreferencesHelper.getInstant().clearPreference();
        AppNavigator.launchSplashScreen(context);
        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.85,
      child: Drawer(
        child: Container(
          color: ColorsHelper.whiteColor(),
          child: SafeArea(
            child: Stack(
              alignment: Alignment.bottomCenter,
              children: <Widget>[
                Column(children: <Widget>[
// ----------------- USER PROFILE DETAILS ------------------------- //
                  // userProfileDetails(),
// ----------------- LIST VIEW BUILDER FOR USER ------------------------- //
                  SizedBox(height: MediaQuery.of(context).size.height * 0.15),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.only(bottom: 57),
                      color: ColorsHelper.whiteColor(),
                      child: ScrollConfiguration(
                        behavior: ListViewScrollBehavior(),
                        child: ListView.builder(
                            padding: EdgeInsets.all(0),
                            itemCount: arrListDetail.length,
                            itemBuilder: (context, index) =>
                                buildTableContainerCell(
                                    arrListDetail[index], index, context)),
                      ),
                    ),
                  ),
                ]),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Container buildTableContainerCell(
      DrawerDetails temp, int index, BuildContext context) {
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 0),
        child: ListTile(
          leading: Container(
              width: 20, height: 20, child: Image.asset(temp.imageCustom)),
          title: textHelper(
            title: temp.titleText,
            textAlign: TextAlign.left,
            fontWeight: FontWeight.w600,
          ),
          onTap: () {
            if (widget.pageId == temp.drawerElementId) {
              AppNavigator.popBackStack(context);
            } else {
              gotToScreen(temp.titleText, context);
            }
          },
        ));
  }
}

// A ListItem that contains data to display a message.
class DrawerDetails {
  final drawerElementId;
  final String titleText;
  final String imageCustom;
  final bool isTrailing;

  DrawerDetails({
    this.drawerElementId,
    this.titleText,
    this.imageCustom,
    this.isTrailing,
  });
}
