import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:teledentistry/src/bloc/bloc_main/main_bloc.dart';
import 'package:teledentistry/src/bloc/bloc_profile/profile_bloc.dart';
import 'package:teledentistry/src/models/UserProfileModel.dart';
import 'package:teledentistry/src/resources/colors.dart';
import 'package:teledentistry/src/resources/image_assets.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/utils/app_navigator/app_navigator.dart';
import 'package:teledentistry/src/utils/common_widgets/common_widget.dart';
import 'package:teledentistry/src/utils/date_picker/datePicker.dart';
import 'package:teledentistry/src/utils/popup_dialogs/popup_dialogs.dart';
import 'package:teledentistry/src/utils/validator/validator.dart';
import 'package:intl/intl.dart';

class EditAccount extends StatefulWidget {
  static const String routeName = "/EditAccount";
  final UserProfileData userProfileData;
  EditAccount({Key key, this.userProfileData}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<EditAccount> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  DateTime dob = DateTime(1987, 11, 24);
  String dateOfBirth = DateFormat('MMM dd yyyy').format(DateTime(1987, 11, 24));

  List<String> gender = ['Male', 'Female'];
  String currentGender;

  TextEditingController nameController = TextEditingController();
  TextEditingController weightController = TextEditingController();
  TextEditingController heightController = TextEditingController();
  TextEditingController insuranceController = TextEditingController();
  TextEditingController policyController = TextEditingController();

  FocusNode nameNode = FocusNode();
  FocusNode weightNode = FocusNode();
  FocusNode heightNode = FocusNode();
  FocusNode insuranceNode = FocusNode();
  FocusNode policyNode = FocusNode();

  MainBloc mainBloc;
  ProfileBloc profileBloc = ProfileBloc();

  File _image;
  final picker = ImagePicker();

  @override
  void initState() {
    mainBloc = BlocProvider.of<MainBloc>(context);
    currentGender = gender[0];
    setdata();

    super.initState();
  }

  setdata() {
    mainBloc.tp();
    nameController.text = widget.userProfileData.profile.fullName;
    weightController.text = widget.userProfileData.profile.weight;
    heightController.text = widget.userProfileData.profile.height;
    insuranceController.text = widget.userProfileData.profile.insurance;
    policyController.text = widget.userProfileData.profile.policy;
    if (widget.userProfileData.profile.gender == "Male") {
      currentGender = gender[0];
    }
    if (widget.userProfileData.profile.dob != '')
      dateOfBirth = widget.userProfileData.profile.dob;
    // DateFormat('MMM dd yyyy')
    //     .format(DateTime.parse(widget.userProfileData.profile.dob));
    if (mounted) setState(() {});
  }

  submit() async {
    var result = await profileBloc.callProfileUpdateApi(
      profileImage: _image,
        fullName: nameController.text.trim(),
        weight: weightController.text.trim(),
        gender: currentGender.trim(),
        height: heightController.text.trim(),
        dob: dateOfBirth.trim(),
        insurance: insuranceController.text.trim(),
        policy: policyController.text.trim(),
        context: context);
    if (result != null) {
      // AppNavigator.launchDashboardScreen(context, null);
      AppNavigator.popBackStack(context);
      PopupDialogs.displayOnlyMessage(context, result);
    }
  }

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callUnfocus(context: context),
      child: Scaffold(
        backgroundColor: ColorsHelper.whiteColor(),
        appBar: appBarBody(
          title: StringHelper.edit_account,
          textColor: ColorsHelper.greyColor(),
          fontWeight: FontWeight.w600,
          isLeading: true,
          onTapLeading: () => AppNavigator.popBackStack(context),
          // action1Icon: ImageAssets.menu_button_5,
          // onTapaction1: () {}
        ),
        body: SingleChildScrollView(child: loginBody()),
        bottomNavigationBar: bottonNavBar(context: context),
      ),
    );
  }

  Widget loginBody() => Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Column(
        children: [
          //vector
          _image == null
              ? InkWell(
                  onTap: () {
                    getImage();
                  },
                  child: Container(
                    width: 120,
                    height: 120,
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(150.0)),
                      child: cacheImage(
                          imgUrl: widget.userProfileData.profile.userPic),
                    ),
                  ),
                )
              : InkWell(
                  onTap: () {
                    getImage();
                  },
                  child: Container(
                    width: 120,
                    height: 120,
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(150.0)),
                      child: Image.file(_image),
                    ),
                  ),
                ),
          SizedBox(height: 20),

          userForm(),
          SizedBox(height: 10),
          //buttons
          buttons(),
          SizedBox(height: 10),
        ],
      ));

  Widget userForm() => Form(
      key: _formKey,
      // autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          //full_Name
          formTextFeild(
            context: context,
            controller: nameController,
            focusNode: nameNode,
            nextFocusNode: weightNode,
            hintText: StringHelper.full_Name,
            textInputType: TextInputType.text,
            textInputAction: TextInputAction.next,
            validation: CommonValidator.emptyValidation,
          ),
          SizedBox(height: 10),

          //weight
          formTextFeild(
            context: context,
            controller: weightController,
            focusNode: weightNode,
            nextFocusNode: heightNode,
            hintText: StringHelper.weight,
            textInputType: TextInputType.text,
            textInputAction: TextInputAction.next,
            validation: CommonValidator.emptyValidation,
          ),
          SizedBox(height: 10),
          //height
          formTextFeild(
            context: context,
            controller: heightController,
            focusNode: heightNode,
            nextFocusNode: insuranceNode,
            hintText: StringHelper.height,
            textInputType: TextInputType.text,
            textInputAction: TextInputAction.next,
            validation: CommonValidator.emptyValidation,
          ),
          SizedBox(height: 10),
          //insurance
          formTextFeild(
            context: context,
            controller: insuranceController,
            focusNode: insuranceNode,
            nextFocusNode: policyNode,
            hintText: StringHelper.insurance,
            textInputType: TextInputType.text,
            textInputAction: TextInputAction.next,
            validation: CommonValidator.emptyValidation,
          ),
          SizedBox(height: 10),
          //policy
          formTextFeild(
            context: context,
            controller: policyController,
            focusNode: policyNode,
            nextFocusNode: null,
            hintText: StringHelper.policy,
            textInputType: TextInputType.text,
            textInputAction: TextInputAction.next,
            validation: CommonValidator.emptyValidation,
          ),
          SizedBox(height: 10),
          //date_of_Birth
          textHelper(title: StringHelper.date_of_Birth, textColor: Colors.grey),
          SizedBox(height: 10),
          //Date of Birth
          textBoxContainer(
              child: Container(
            height: 50,
            alignment: Alignment.centerLeft,
            child: GestureDetector(
                onTap: () {
                  callUnfocus(context: context);
                  DatePickerNew dpn = DatePickerNew();
                  dpn.date = dob;
                  dpn.pickDate(context,
                      initDate: dob ?? DateTime(1987, 11, 24),
                      minYear: DateTime.now().year - 80,
                      maxYear: DateTime.now().year, doneFunc: () {
                    setState(() => dob = dpn.date);
                    dateOfBirth = DateFormat('MMM dd yyyy').format(dob);
                  }).then((onValue) => FocusScope.of(context).unfocus());
                },
                child: Container(
                    padding: EdgeInsets.all(0), child: Text(dateOfBirth))),
          )),
          SizedBox(height: 10),
          //gender
          textHelper(title: StringHelper.gender, textColor: Colors.grey),
          SizedBox(height: 10),
          //currentGender
          DropdownButton<String>(
            isExpanded: true,
            icon: Icon(Icons.arrow_drop_down),
            underline: Container(),
            isDense: true,
            items: gender.map((String value) {
              return DropdownMenuItem(
                value: value,
                child: Text(
                  value,
                  style: TextStyle(fontSize: 14),
                ),
              );
            }).toList(),
            onChanged: (String newValueSelected) {
              setState(() {
                currentGender = newValueSelected;
              });
            },
            value: currentGender,
          ),
          SizedBox(height: 10),
        ],
      ));

  Widget buttons() => Row(
        children: [
          //reset_Password
          Expanded(
              child: submitButton(
                  onPressed: () {
                    callUnfocus(context: context);
                    profileBloc.onLoginClick(_formKey).then((value) {
                      if (value) {
                        submit();
                      } else {
                        if (mounted) {
                          setState(() {});
                        }
                      }
                    });
                  },
                  title: StringHelper.update)),
        ],
      );

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);
    setState(() {
      _image = File(pickedFile.path);
    });
  }
}
