import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teledentistry/src/bloc/bloc_login/login_bloc.dart';
import 'package:teledentistry/src/bloc/bloc_main/main_bloc.dart';
import 'package:teledentistry/src/data/network/api.dart';
import 'package:teledentistry/src/models/UrlLinksModel.dart';
import 'package:teledentistry/src/resources/colors.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/utils/app_navigator/app_navigator.dart';
import 'package:teledentistry/src/utils/common_widgets/common_widget.dart';

class ConsentFormPatient extends StatefulWidget {
  static const String routeName = "/ConsentFormPatient";
  @override
  _ConsentFormPatientState createState() => _ConsentFormPatientState();
}

class _ConsentFormPatientState extends State<ConsentFormPatient> {
  UrlLinksData urlData;
  LoginBloc loginBloc = LoginBloc();
  MainBloc mainBloc;
  @override
  void initState() {
    callData();
    super.initState();
  }

  callData() async {
    mainBloc = BlocProvider.of<MainBloc>(context);
    mainBloc.tp();
    urlData = await loginBloc.callUrlLinksApi(
        context: context, apiLink: Api.consent_form);
    if (urlData != null && mounted) setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.5,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                  ColorsHelper.blueColor(),
                  ColorsHelper.lightBlueColor()
                ])),
          ),
          Scaffold(
            backgroundColor: Colors.transparent,
            appBar: appBarBody(
              title: StringHelper.consent_Forms,
              textColor: ColorsHelper.whiteColor(),
              fontWeight: FontWeight.w600,
              isLeading: true,
              onTapLeading: () => AppNavigator.popBackStack(context),
              leadingColor: ColorsHelper.whiteColor(),
            ),
            body: consentFormPatientBody(),
            bottomNavigationBar: bottonNavBar(context: context),
          )
        ],
      ),
    );
  }

  Widget consentFormPatientBody() => Column(
        children: [
          SizedBox(height: MediaQuery.of(context).size.height * 0.1),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: ColorsHelper.whiteColor(),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: SingleChildScrollView(child: textBody()),
            ),
          ),
        ],
      );

  Widget textBody() => Column(
        children: [
          textHelper(
              textAlign: TextAlign.left,
              title: StringHelper.patient_consent_forms),
          SizedBox(height: 10),
          Container(
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: submitButton(
                  onPressed: () {
                    Navigator.of(context).pop(true);
                    Navigator.of(context).pop(true);
                  },
                  title: StringHelper.i_agree)),
          SizedBox(height: 10),
        ],
      );
}
