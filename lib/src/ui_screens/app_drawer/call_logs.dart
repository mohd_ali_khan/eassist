import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:teledentistry/src/data/network/api.dart';
import 'package:teledentistry/src/data/network/api_response.dart';
import 'package:teledentistry/src/data/network/rest_client_new.dart';
import 'package:teledentistry/src/models/CallLogsModel.dart';
import 'package:teledentistry/src/resources/colors.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/utils/app_navigator/app_navigator.dart';
import 'package:intl/intl.dart';
import 'package:teledentistry/src/utils/common_widgets/common_widget.dart';
import 'package:teledentistry/src/utils/listview_scroll_behavior/listview_scroll_behavior.dart';
import 'package:teledentistry/src/utils/sharedpreference_helper/sharepreference_helper.dart';

class CallLogs extends StatefulWidget {
  static const String routeName = "/CallLogs";
  @override
  _CallLogsState createState() => _CallLogsState();
}

class _CallLogsState extends State<CallLogs> {
  ScrollController _scrollController = new ScrollController();
  bool isLoading = false;
  CallLogsData callLogsData = CallLogsData();
  List<LogDetails> logDetailsList = List<LogDetails>();
  final dio = new Dio();
  int count = 0;

  void _getMoreData() async {
    if (!isLoading && mounted) {
      setState(() {
        isLoading = true;
      });
      var token = await SharePreferencesHelper.getInstant()
          .getAccessToken(SharePreferencesHelper.Access_Token);
      var body = {"page_no": count};
      List<LogDetails> tempList = new List<LogDetails>();
      ApiResponse apiResponse = await restClientNew.post(context, Api.get_log,
          body: body, header: {'version': '1.0.0', 'Authorization': token});
      if (apiResponse.status == 1) {
        callLogsData = CallLogsData.fromJson(apiResponse.data[0]);
        count++;
        for (int i = 0; i < callLogsData.logDetails.length; i++) {
          tempList.add(callLogsData.logDetails[i]);
        }
      }
      setState(() {
        isLoading = false;
        logDetailsList.addAll(tempList);
      });
    }
  }

  @override
  void initState() {
    this._getMoreData();
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _getMoreData();
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    logDetailsList.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.5,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                  ColorsHelper.blueColor(),
                  ColorsHelper.lightBlueColor()
                ])),
          ),
          Scaffold(
            backgroundColor: Colors.transparent,
            appBar: appBarBody(
              title: StringHelper.call_Log,
              textColor: ColorsHelper.whiteColor(),
              fontWeight: FontWeight.w600,
              isLeading: true,
              onTapLeading: () => AppNavigator.popBackStack(context),
              leadingColor: ColorsHelper.whiteColor(),
              // action1Icon: ImageAssets.menu_button_4,
              // onTapaction1: () {}
            ),
            body: callLogsBody(),
            bottomNavigationBar: bottonNavBar(context: context),
          )
        ],
      ),
    );
  }

  Widget callLogsBody() => Column(
        children: [
          SizedBox(height: MediaQuery.of(context).size.height * 0.1),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: ColorsHelper.whiteColor(),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: patientsSearchBody(),
            ),
          ),
        ],
      );

  Widget patientsSearchBody() => Column(
        children: [
          // //Search , filter
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //   children: [
          //     //search
          //     textHelper(
          //         fontSize: 13,
          //         textColor: ColorsHelper.greyColor(),
          //         textAlign: TextAlign.end,
          //         title: StringHelper.search),
          //     InkWell(
          //       borderRadius: BorderRadius.circular(50),
          //       onTap: () {},
          //       child: Container(
          //           width: 35,
          //           padding: EdgeInsets.all(5),
          //           child: Image.asset(
          //             ImageAssets.filter_button,
          //           )),
          //     )
          //   ],
          // ),
          Flexible(
            child: ScrollConfiguration(
              behavior: ListViewScrollBehavior(),
              child: ListView.builder(
                  shrinkWrap: true,
                  controller: _scrollController,
                  itemCount: logDetailsList.length + 1,
                  itemBuilder: (context, index) {
                    if (index == logDetailsList.length) {
                      return _buildProgressIndicator();
                    } else {
                      return listBox(logDetails: logDetailsList[index]);
                    }
                  }),
            ),
          )
        ],
      );

  Widget _buildProgressIndicator() {
    return new Padding(
      padding: const EdgeInsets.all(8.0),
      child: new Center(
        child: new Opacity(
          opacity: isLoading ? 0.0 : 00,
          child: new CircularProgressIndicator(),
        ),
      ),
    );
  }

  Widget listBox({LogDetails logDetails}) => Container(
      margin: EdgeInsets.symmetric(vertical: 5),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: ColorsHelper.greyTextBoxColor(),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //user name
          textHelper(
              title: logDetails.name ?? "Jakeson",
              fontSize: 16,
              fontWeight: FontWeight.w600),
          SizedBox(height: 5),
          //number
          textHelper(
              title: logDetails.number ?? "89562156525",
              textColor: ColorsHelper.greyColor()),
          SizedBox(height: 20),
          listBoxHelper(
              title: StringHelper.date_of_call,
              value: DateFormat('yMd')
                  .format(DateTime.parse(logDetails.dtOfCall))),
          listBoxHelper(
              title: StringHelper.time_of_call,
              value: DateFormat('jm')
                  .format(DateTime.parse(logDetails.timeOfCall))),
          listBoxHelper(
              title: StringHelper.end_of_call,
              value: DateFormat('jm')
                  .format(DateTime.parse(logDetails.endOfCall))),
          // listBoxHelper(
          // title: StringHelper.length_of_call, value: logDetails.),
          listBoxHelper(
              title: StringHelper.purpose_of_call,
              value: logDetails.purposeCall),
          listBoxHelper(
              title: StringHelper.follow_up_neeeded,
              value: logDetails.followupNeeded),
        ],
      ));

  Widget listBoxHelper({String title, String value}) => Container(
        padding: EdgeInsets.only(top: 5),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
                child: textHelper(
                    textAlign: TextAlign.left, title: title, fontSize: 14)),
            Expanded(
                child: textHelper(
                    textAlign: TextAlign.right, title: value, fontSize: 14)),
          ],
        ),
      );
}
