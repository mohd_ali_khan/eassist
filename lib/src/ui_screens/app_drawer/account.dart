import 'package:flutter/material.dart';
import 'package:teledentistry/src/bloc/bloc_profile/profile_bloc.dart';
import 'package:teledentistry/src/models/UserProfileModel.dart';
import 'package:teledentistry/src/resources/colors.dart';
import 'package:teledentistry/src/resources/image_assets.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/utils/app_navigator/app_navigator.dart';
import 'package:teledentistry/src/utils/common_widgets/common_widget.dart';

import 'edit_account.dart';

class Account extends StatefulWidget {
  static const String routeName = "/Account";
  @override
  _AccountState createState() => _AccountState();
}

class _AccountState extends State<Account> {
  ProfileBloc profileBloc = ProfileBloc();
  UserProfileData userProfileData;
  @override
  void initState() {
    super.initState();
    getData();
  }

  Future getData() async {
    userProfileData = await profileBloc.callGetUserProfileApi(
      context: context,
    );
    if (mounted) setState(() {});
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height * 0.5,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                  ColorsHelper.blueColor(),
                  ColorsHelper.lightBlueColor()
                ])),
          ),
          Scaffold(
            backgroundColor: Colors.transparent,
            appBar: appBarBody(
              title: StringHelper.account,
              textColor: ColorsHelper.whiteColor(),
              fontWeight: FontWeight.w600,
              isLeading: true,
              onTapLeading: () => AppNavigator.popBackStack(context),
              leadingColor: ColorsHelper.whiteColor(),
            ),
            body: userProfileData != null ? consentFormBody() : null,
            bottomNavigationBar: bottonNavBar(context: context),
          )
        ],
      ),
    );
  }

  Widget consentFormBody() => Column(
        children: [
          SizedBox(height: 30),
          profileBody(),
          SizedBox(height: 30),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: ColorsHelper.whiteColor(),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20))),
              child: SingleChildScrollView(child: textBody()),
            ),
          ),
        ],
      );

  Widget profileBody() => Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: 120,
              height: 120,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(150.0)),
                child: cacheImage(imgUrl: userProfileData.profile.userPic),
              ),
            ),
            SizedBox(width: 10),
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  textHelper(
                      textAlign: TextAlign.left,
                      title: userProfileData.profile.fullName,
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      textColor: ColorsHelper.whiteColor()),
                  textHelper(
                      textAlign: TextAlign.left,
                      title: userProfileData.profile.userPhone,
                      fontSize: 16,
                      textColor: ColorsHelper.whiteColor()),
                  textHelper(
                      textAlign: TextAlign.left,
                      title: userProfileData.profile.userEmail,
                      fontSize: 16,
                      textColor: ColorsHelper.whiteColor()),
                  SizedBox(height: 10),
                  //Edit account
                  Row(
                    children: [
                      InkWell(
                        borderRadius: BorderRadius.circular(50),
                        onTap: () {
                          // AppNavigator.launchEditAccountScreen(
                          //     context, userProfileData);
                          Navigator.pushNamed(context, EditAccount.routeName,
                                  arguments: userProfileData)
                              .then((value) {
                            getData();
                          });
                        },
                        child: Container(
                            width: 35,
                            padding: EdgeInsets.all(5),
                            child: Image.asset(
                              ImageAssets.group,
                            )),
                      ),
                      SizedBox(width: 5),
                      //edit_account
                      textHelper(
                          fontSize: 13,
                          textColor: ColorsHelper.whiteColor(),
                          textAlign: TextAlign.start,
                          title: StringHelper.edit_account),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      );

  Widget textBody() => Column(
        children: [
          textRowHelper(
              onTap: () {},
              title: StringHelper.gender,
              value: userProfileData.profile.gender),
          line(),
          textRowHelper(
              onTap: () {},
              title: StringHelper.birthdate,
              value: userProfileData.profile.dob),
          line(),
          textRowHelper(
              onTap: () {},
              title: StringHelper.height,
              value: userProfileData.profile.height),
          line(),
          textRowHelper(
              onTap: () {},
              title: StringHelper.weight,
              value: userProfileData.profile.weight),
          line(),
          textRowHelper(
              onTap: () {},
              title: StringHelper.insurance,
              value: userProfileData.profile.insurance),
          line(),
          textRowHelper(
              onTap: () {},
              title: StringHelper.policy,
              value: userProfileData.profile.policy),
          line(),
        ],
      );

  Widget textRowHelper({String title, String value, Function onTap}) =>
      Material(
        color: Colors.transparent,
        borderRadius: BorderRadius.circular(5),
        child: InkWell(
            borderRadius: BorderRadius.circular(5),
            onTap: onTap,
            child: Container(
              color: Colors.transparent,
              padding: EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  textHelper(title: title),
                  textHelper(title: value),
                ],
              ),
            )),
      );
}
