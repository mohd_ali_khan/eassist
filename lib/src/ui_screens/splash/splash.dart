import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teledentistry/src/bloc/bloc_main/main_bloc.dart';
import 'package:teledentistry/src/models/VersionCheckModel.dart';
import 'package:teledentistry/src/resources/colors.dart';
import 'package:teledentistry/src/resources/image_assets.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/utils/app_navigator/app_navigator.dart';
import 'package:teledentistry/src/utils/common_widgets/common_widget.dart';
import 'package:teledentistry/src/utils/firebase_ops/fcm.dart';
import 'package:teledentistry/src/utils/popup_dialogs/popup_dialogs.dart';
import 'package:teledentistry/src/utils/sharedpreference_helper/sharepreference_helper.dart';
import 'package:url_launcher/url_launcher.dart';

class Splash extends StatefulWidget {
  static const String routeName = "/";
  Splash({Key key}) : super(key: key);

  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  MainBloc mainBloc;
  bool isLogin = false;

  List<String> chooseAppType = ['I am a', 'I am a Doctor', 'I am a Patient'];

  String currentAppType;

  @override
  void initState() {
    mainBloc = BlocProvider.of<MainBloc>(context);
    currentAppType = chooseAppType[0];
    callVersionCheck();
    super.initState();
  }

  callVersionCheck() async {
    VersionCheckData result =
        await mainBloc.callVersionCheckApi(context: context);
    print(result);
    if (Platform.isIOS) {
      if (result.version.currentVersion.contains("2.0.0")) {
        callData();
      } else
        PopupDialogs.displayAppUpdate(
            context: context,
            message: result.version.versionmessage,
            onTap: () async {
              if (await canLaunch(result.version.appStore)) {
                await launch(result.version.appStore);
              } else {
                throw 'Could not launch ${result.version.appStore}';
              }
            });
    } else {
      if (result.version.currentVersion.contains("1.0.0")) {
        callData();
      } else
        PopupDialogs.displayAppUpdate(
            context: context,
            message: result.version.versionmessage,
            onTap: () async {
              if (await canLaunch(result.version.playStore)) {
                await launch(result.version.playStore);
              } else {
                throw 'Could not launch ${result.version.playStore}';
              }
            });
    }
  }

  callData() async {
    mainBloc.tp();
    mainBloc.fbToken = await Fcm.getInstance().getFbToken();
    Future.delayed(Duration(milliseconds: 0), () async {
      launchLoginOrDashboardPage(context);
    });
  }

  void launchLoginOrDashboardPage(BuildContext context) async {
    isLogin = await SharePreferencesHelper.getInstant()
        .getBool(SharePreferencesHelper.Is_Login);
    if (isLogin) {
      AppNavigator.launchDashboardScreen(context, null);
    }
  }

  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
          bottomNavigationBar: bottonNavBar(context: context),
          backgroundColor: ColorsHelper.whiteColor(),
          body: SafeArea(
            child: Container(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 50),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    //teledentistry
                    Container(
                        height: MediaQuery.of(context).size.height * 0.080,
                        child: assetImageHelper(
                          image: ImageAssets.teledentistry,
                        )),
                    //vector
                    Container(
                      height: MediaQuery.of(context).size.height * 0.2,
                      child: assetImageHelper(
                        image: ImageAssets.vector,
                      ),
                    ),
                    //doctor , patient
                    Visibility(
                      visible: chooseAppType.length != 0,
                      child: DropdownButton<String>(
                        isExpanded: true,
                        icon: Icon(Icons.arrow_drop_down),
                        items: chooseAppType.map((String value) {
                          return DropdownMenuItem(
                            value: value,
                            child: Text(
                              value,
                              style: TextStyle(fontSize: 14),
                            ),
                          );
                        }).toList(),
                        onChanged: (String newValueSelected) {
                          callUnfocus(context: context);
                          setState(() {
                            currentAppType = newValueSelected;
                          });
                        },
                        value: currentAppType,
                      ),
                    ),
                    if (currentAppType != chooseAppType[0])
                      submitButton(
                          onPressed: () {
                            //2 for doctor, 3 for patient
                            if (currentAppType == chooseAppType[1]) {
                              mainBloc.userType = "2";
                            } else if (currentAppType == chooseAppType[2]) {
                              mainBloc.userType = "3";
                            }
                            AppNavigator.launchLoginScreen(context);
                          },
                          title: StringHelper.launch),
                    // //group_1
                    // Container(
                    //   height: MediaQuery.of(context).size.height * 0.085,
                    //   child: assetImageHelper(
                    //     image: ImageAssets.group_1,
                    //   ),
                    // )
                  ],
                )),
          )),
    );
  }
}
