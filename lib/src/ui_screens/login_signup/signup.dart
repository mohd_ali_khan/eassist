import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teledentistry/src/bloc/bloc_login/login_bloc.dart';
import 'package:teledentistry/src/bloc/bloc_main/main_bloc.dart';
import 'package:teledentistry/src/resources/colors.dart';
import 'package:teledentistry/src/resources/image_assets.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/ui_screens/app_drawer/consent_forms.dart';
import 'package:teledentistry/src/utils/app_navigator/app_navigator.dart';
import 'package:teledentistry/src/utils/common_widgets/common_widget.dart';
import 'package:teledentistry/src/utils/sharedpreference_helper/sharepreference_helper.dart';
import 'package:teledentistry/src/utils/validator/validator.dart';

class Signup extends StatefulWidget {
  static const String routeName = "/Signup";
  Signup({Key key}) : super(key: key);

  @override
  _SignupState createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isHide = true;

  TextEditingController emailController = TextEditingController();
  TextEditingController passswordController = TextEditingController();
  TextEditingController fullNameController = TextEditingController();
  TextEditingController phoneNoController = TextEditingController();

  FocusNode emailNode = FocusNode();
  FocusNode passwordNode = FocusNode();
  FocusNode fullNameNode = FocusNode();
  FocusNode phoneNoNode = FocusNode();

  MainBloc mainBloc;
  LoginBloc loginBloc = LoginBloc();

  @override
  void initState() {
    mainBloc = BlocProvider.of<MainBloc>(context);
    super.initState();
  }

  callData() {
    mainBloc.tp();
  }

  submit() async {
    var result = await loginBloc.callRegistrationApi(
        fullName: fullNameController.text.trim(),
        email: emailController.text.trim(),
        password: passswordController.text.trim(),
        phoneNo: phoneNoController.text.trim(),
        type: mainBloc.userType,
        deviceId: mainBloc.fbToken,
        context: context);
    if (result) {
      SharePreferencesHelper.getInstant()
          .setBool(SharePreferencesHelper.Is_Login, true);
      AppNavigator.launchDashboardScreen(context, null);
    }
  }

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callUnfocus(context: context),
      child: Scaffold(
        backgroundColor: ColorsHelper.whiteColor(),
        appBar: appBarBody(
          title: StringHelper.sign_up,
          textColor: ColorsHelper.greyColor(),
          fontWeight: FontWeight.w600,
          isLeading: true,
          onTapLeading: () => AppNavigator.popBackStack(context),
          // action1Icon: ImageAssets.menu_button_5,
          // onTapaction1: () {}
        ),
        body: SingleChildScrollView(child: signupBody()),
        bottomNavigationBar: bottonNavBar(context: context),
      ),
    );
  }

  Widget signupBody() => Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Column(
        children: [
          //vector
          Container(
            height: MediaQuery.of(context).size.height * 0.2,
            child: assetImageHelper(
              image: ImageAssets.vector,
            ),
          ),
          userForm(),
          SizedBox(height: 10),
          //buttons
          buttons(),
          SizedBox(height: 10),
          //already_have_an_account
          textWithLink(
            text1: StringHelper.already_have_an_account,
            text2: StringHelper.login,
            ontap: () => AppNavigator.popBackStack(context),
          ),
        ],
      ));

  Widget userForm() => Form(
      key: _formKey,
      // autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          //full_Name
          formTextFeild(
              context: context,
              controller: fullNameController,
              focusNode: fullNameNode,
              nextFocusNode: phoneNoNode,
              hintText: StringHelper.full_Name,
              textInputType: TextInputType.text,
              textInputAction: TextInputAction.next,
              validation: CommonValidator.emptyValidation),
          SizedBox(height: 10),
          //mobile_Phone
          formTextFeild(
              context: context,
              controller: phoneNoController,
              focusNode: phoneNoNode,
              nextFocusNode: emailNode,
              hintText: StringHelper.mobile_Phone,
              textInputType: TextInputType.phone,
              textInputAction: TextInputAction.next,
              maxLength: 15,
              validation: CommonValidator.emptyValidation),
          SizedBox(height: 10),
          //email_ID
          formTextFeild(
              context: context,
              controller: emailController,
              focusNode: emailNode,
              nextFocusNode: passwordNode,
              hintText: StringHelper.email_Address,
              textInputType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              validation: CommonValidator.emailValidation),
          SizedBox(height: 10),
          //password
          formTextFeild(
              context: context,
              controller: passswordController,
              focusNode: passwordNode,
              nextFocusNode: null,
              hintText: StringHelper.password,
              textInputType: TextInputType.text,
              textInputAction: TextInputAction.done,
              validation: CommonValidator.passValidation,
              obscureText: isHide,
              suffixIcon: true,
              suffixFuncton: () {
                if (mounted) {
                  isHide = !isHide;
                  setState(() {});
                }
              }),
          SizedBox(height: 10),
        ],
      ));

  Widget buttons() => Row(
        children: [
          //Signup
          Expanded(
              child: submitButton(
                  onPressed: () {
                    callUnfocus(context: context);
                    loginBloc.onLoginClick(_formKey).then((value) {
                      if (value)
                        Navigator.of(context)
                            .pushNamed(ConsentForm.routeName)
                            .then((onValue) {
                          if (onValue) submit();
                        });
                      else if (mounted) setState(() {});
                    });
                  },
                  title: StringHelper.sign_up)),
        ],
      );
}
