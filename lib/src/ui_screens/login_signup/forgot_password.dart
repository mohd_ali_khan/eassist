import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:teledentistry/src/bloc/bloc_login/login_bloc.dart';
import 'package:teledentistry/src/bloc/bloc_main/main_bloc.dart';
import 'package:teledentistry/src/resources/colors.dart';
import 'package:teledentistry/src/resources/image_assets.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/utils/app_navigator/app_navigator.dart';
import 'package:teledentistry/src/utils/common_widgets/common_widget.dart';
import 'package:teledentistry/src/utils/popup_dialogs/popup_dialogs.dart';
import 'package:teledentistry/src/utils/validator/validator.dart';

class ForgotPassword extends StatefulWidget {
  static const String routeName = "/ForgotPassword";
  ForgotPassword({Key key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<ForgotPassword> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isHide = true;

  TextEditingController emailController = TextEditingController();
  TextEditingController passswordController = TextEditingController();

  FocusNode emailNode = FocusNode();
  FocusNode passwordNode = FocusNode();

  MainBloc mainBloc;
  LoginBloc loginBloc = LoginBloc();

  bool isRequested = false;

  @override
  void initState() {
    mainBloc = BlocProvider.of<MainBloc>(context);
    super.initState();
  }

  callData() {
    mainBloc.tp();
  }

  submit() async {
    var result = await loginBloc.callForgetPasswordApi(
        email: emailController.text.trim(), context: context);
    if (result != null) {
      AppNavigator.popBackStack(context);
      PopupDialogs.displayOnlyMessage(context,
          "We have shared a new password on your registered email id please use it for login. Thanks");
      if (mounted) {
        setState(() {
          isRequested = true;
        });
      }
    }
  }

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callUnfocus(context: context),
      child: Scaffold(
        backgroundColor: ColorsHelper.whiteColor(),
        appBar: appBarBody(
          title: StringHelper.forgot_Password,
          textColor: ColorsHelper.greyColor(),
          fontWeight: FontWeight.w600,
          isLeading: true,
          onTapLeading: () => AppNavigator.popBackStack(context),
          // action1Icon: ImageAssets.menu_button_5,
          // onTapaction1: () {}
        ),
        body: SingleChildScrollView(child: loginBody()),
        bottomNavigationBar: bottonNavBar(context: context),
      ),
    );
  }

  Widget loginBody() => Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Column(
        children: [
          //vector
          Container(
            height: MediaQuery.of(context).size.height * 0.2,
            child: assetImageHelper(
              image: ImageAssets.vector,
            ),
          ),
          SizedBox(height: 20),
          //text
          textHelper(
              fontSize: isRequested ? 17 : 13,
              textColor: ColorsHelper.greyColor(),
              textAlign: TextAlign.start,
              title: isRequested
                  ? StringHelper.reset_email_sent_to_your_mailbox
                  : StringHelper.please_enter_your_email_address),
          userForm(),
          SizedBox(height: 10),
          //buttons
          buttons(),
          SizedBox(height: 10),
        ],
      ));

  Widget userForm() => Form(
      key: _formKey,
      // autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          //email_ID
          formTextFeild(
              context: context,
              controller: emailController,
              focusNode: emailNode,
              nextFocusNode: passwordNode,
              hintText: StringHelper.email_Address,
              textInputType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              validation: CommonValidator.emailValidation),
          SizedBox(height: 10),
        ],
      ));

  Widget buttons() => Row(
        children: [
          //reset_Password
          Expanded(
              child: submitButton(
                  onPressed: () {
                    callUnfocus(context: context);
                    loginBloc.onLoginClick(_formKey).then((value) {
                      if (value) {
                        submit();
                      } else {
                        if (mounted) {
                          setState(() {});
                        }
                      }
                    });
                  },
                  title: isRequested
                      ? StringHelper.send_again
                      : StringHelper.reset_Password)),
        ],
      );
}
