import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sign_in_with_apple/sign_in_with_apple.dart';
import 'package:teledentistry/src/bloc/bloc_login/login_bloc.dart';
import 'package:teledentistry/src/bloc/bloc_main/main_bloc.dart';
import 'package:teledentistry/src/resources/colors.dart';
import 'package:teledentistry/src/resources/image_assets.dart';
import 'package:teledentistry/src/resources/strings.dart';
import 'package:teledentistry/src/ui_screens/app_drawer/consent_forms.dart';
import 'package:teledentistry/src/utils/app_navigator/app_navigator.dart';
import 'package:teledentistry/src/utils/common_widgets/common_widget.dart';
import 'package:teledentistry/src/utils/sharedpreference_helper/sharepreference_helper.dart';
import 'package:teledentistry/src/utils/validator/validator.dart';

class Login extends StatefulWidget {
  static const String routeName = "/Login";
  Login({Key key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool isHide = true;

  TextEditingController emailController = TextEditingController();
  TextEditingController passswordController = TextEditingController();

  FocusNode emailNode = FocusNode();
  FocusNode passwordNode = FocusNode();

  MainBloc mainBloc;
  LoginBloc loginBloc = LoginBloc();

  @override
  void initState() {
    mainBloc = BlocProvider.of<MainBloc>(context);
    super.initState();
  }

  submit() async {
    mainBloc.tp();
    var result = await loginBloc.callLoginApi(
        email: emailController.text.trim(),
        password: passswordController.text.trim(),
        deviceId: mainBloc.fbToken,
        context: context);
    if (result) {
      SharePreferencesHelper.getInstant()
          .setBool(SharePreferencesHelper.Is_Login, true);
      AppNavigator.launchDashboardScreen(context, null);
    }
  }

  final GoogleSignIn googleSignIn = GoogleSignIn();
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  SharedPreferences prefs;

  User currentUser;

  Future<Null> handleSignIn() async {
    googleSignIn.signOut();
    GoogleSignInAccount googleUser = await googleSignIn.signIn();
    socialSubmit(
        id: googleUser.id,
        name: googleUser.displayName,
        email: googleUser.email);
  }

  socialSubmit({String id, String name, String email}) async {
    var result = await loginBloc.callSocialLoginApi(
        socialId: id,
        fullName: name,
        deviceId: mainBloc.fbToken,
        type: mainBloc.userType,
        email: email,
        context: context);
    if (result) {
      SharePreferencesHelper.getInstant()
          .setBool(SharePreferencesHelper.Is_Login, true);
      AppNavigator.launchDashboardScreen(context, null);
    }
  }

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callUnfocus(context: context),
      child: Scaffold(
        backgroundColor: ColorsHelper.whiteColor(),
        appBar: appBarBody(
          title: StringHelper.login,
          textColor: ColorsHelper.greyColor(),
          fontWeight: FontWeight.w600,
          isLeading: true,
          onTapLeading: () => AppNavigator.popBackStack(context),
        ),
        body: SingleChildScrollView(child: loginBody()),
        bottomNavigationBar: bottonNavBar(context: context),
      ),
    );
  }

  Widget loginBody() => Container(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Column(
        children: [
          //vector
          Container(
            height: MediaQuery.of(context).size.height * 0.2,
            child: assetImageHelper(
              image: ImageAssets.vector,
            ),
          ),
          SizedBox(height: 10),
          userForm(),
          SizedBox(height: 10),
          //buttons
          buttons(),

          SizedBox(height: 10),
          //need_an_account
          textWithLink(
            text1: StringHelper.need_an_account,
            text2: StringHelper.sign_up,
            ontap: () => AppNavigator.launchSignupScreen(context),
          ),
          SizedBox(height: 5),
          //forgot_your_password
          textWithLink(
            text1: StringHelper.forgot_your_password,
            text2: StringHelper.reset,
            ontap: () => AppNavigator.launchForgotPasswordScreen(context),
          )
        ],
      ));

  Widget userForm() => Form(
      key: _formKey,
      // autovalidateMode: AutovalidateMode.onUserInteraction,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          //email_ID
          formTextFeild(
              context: context,
              controller: emailController,
              focusNode: emailNode,
              nextFocusNode: passwordNode,
              hintText: StringHelper.email_Address,
              textInputType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              validation: CommonValidator.emailValidation),
          SizedBox(height: 10),
          //password
          formTextFeild(
              context: context,
              controller: passswordController,
              focusNode: passwordNode,
              nextFocusNode: null,
              hintText: StringHelper.password,
              textInputType: TextInputType.text,
              textInputAction: TextInputAction.done,
              validation: CommonValidator.passValidation,
              obscureText: isHide,
              suffixIcon: true,
              suffixFuncton: () {
                if (mounted) {
                  isHide = !isHide;
                  setState(() {});
                }
              }),
          SizedBox(height: 10),
        ],
      ));

  Widget buttons() => Row(
        children: [
          //Login
          Expanded(
              child: submitButton(
                  onPressed: () {
                    callUnfocus(context: context);
                    loginBloc.onLoginClick(_formKey).then((value) {
                      if (value)
                        submit();
                      else if (mounted) setState(() {});
                    });
                  },
                  title: StringHelper.login)),
          SizedBox(width: 5),
          //google
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Platform.isAndroid ?
              Flexible(
                  child: textHelper(
                      fontSize: 13,
                      textColor: ColorsHelper.greyColor(),
                      textAlign: TextAlign.end,
                      title: StringHelper.or_sign_in_with_google)) : Container(),
              SizedBox(width: 5),
              //goolge button
              InkWell(
                borderRadius: BorderRadius.circular(10),
                onTap: () {
                  callUnfocus(context: context);
                  Navigator.of(context)
                      .pushNamed(ConsentForm.routeName)
                      .then((onValue) {
                    if (onValue) handleSignIn();
                  });
                },
                child: assetImageHelper(
                  height: 44,
                  image: ImageAssets.google,
                ),
              ),

              SizedBox(width: 5),
              //Apple button
              Platform.isIOS ? InkWell(
                onTap: () async{
                    final credential =
                        await SignInWithApple.getAppleIDCredential(
                      scopes: [
                        AppleIDAuthorizationScopes.email,
                        AppleIDAuthorizationScopes.fullName,
                      ],
                      webAuthenticationOptions: WebAuthenticationOptions(
                        // TODO: Set the `clientId` and `redirectUri` arguments to the values you entered in the Apple Developer portal during the setup
                        clientId:
                            'com.aboutyou.dart_packages.sign_in_with_apple.example',
                        redirectUri: Uri.parse(
                          'https://flutter-sign-in-with-apple-example.glitch.me/callbacks/sign_in_with_apple',
                        ),
                      ),
                      // TODO: Remove these if you have no need for them
                      nonce: 'example-nonce',
                      state: 'example-state',
                    );

                    print(credential);

                    socialSubmit(
                        id: credential.identityToken,
                        name:
                            credential.givenName + ' ' + credential.familyName,
                        email: credential.email);
                  },
                child: Container(
                  height: 44,
                  width: 44,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.black,
                      image: DecorationImage(
                          image: AssetImage('assets/applelogo3.png'))),
                ),
              ) : Container()
            ],
          )),
          //apple
        ],
      );
}
