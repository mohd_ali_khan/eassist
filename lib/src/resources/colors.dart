import 'package:flutter/material.dart';

class ColorsHelper {
  static Color blueColor() => Color(0xff004792);

  static Color greyColor() => Color(0xff8D98A4);

  static Color lightBlueColor() => Color(0xff28C2E4);

  static Color greyTextBoxColor() => Color(0xffEBEBEB);

  static Color greyHomeColor() => Color(0xffE1E0FF);

  static Color whiteColor() => Colors.white;

  static Color blackColor() => Colors.black;

  static Color greenColor() => Color(0xff00A652);

  static Color redColor() => Color(0xffEA1D27);

  static Color specialistColor() => Color(0xffBDDFFA);
}
