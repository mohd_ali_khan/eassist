class ImageAssets {
  //Teledentistry

  //Splash
  static const teledentistry = 'assets/Teledentistry-2.png';
  static const vector = 'assets/Vector-2.png';
  static const group_1 = 'assets/Group 1-2.png';

  //Login
  static const password_show = 'assets/password-show-3x.png';
  static const password_hide = 'assets/password-hide-3x.png';
  static const google = 'assets/Twitter Button.png';

  //Dashboard
  static const shape = 'assets/Shape.png';
  static const menu_button_5 = 'assets/Menu button-5.png';
  static const menu_button_4 = 'assets/Menu button-4.png';
  static const group_1_ = 'assets/Group 1-3.png';

  //Patient Search
  static const filter_button = 'assets/filter button.png';

  //Patient Info
  static const message_square = 'assets/message-square.png';
  static const at_rate = 'assets/At_rate.png';
  static const vector_ = 'assets/Vector.png';

  //video calling
  static const faceTime = 'assets/FaceTime.png';
  static const group_52 = 'assets/Group 52.png';
  static const group_35 = 'assets/Group 35.png';

  //AppDrawer
  static const appD1 = 'assets/appD1.png';
  static const appD2 = 'assets/appD2.png';
  static const appD3 = 'assets/appD3.png';
  static const appD4 = 'assets/appD4.png';
  static const appD5 = 'assets/appD5.png';
  static const appD6 = 'assets/appD6.png';
  static const appD7 = 'assets/appD7.png';

  //Accounts
  static const group = 'assets/Group.png';
}
