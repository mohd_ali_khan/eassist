class StringHelper {
  static const app_name = 'Teledentistry';

  //Rest_Client_New
  static const session_expired_please = "Session expired please login again";
  static const server_error_please_try_again_later =
      "Server error please try again later!";

  static const aPI_Crashed = "API Crashed";
  static const press_back_again_to_exit = "Press back again to exit";

  //
  static const no_Internet_Connection = "No Internet Connection";
  static const please_try_to_connect_with_internet =
      "Please try to connect with internet";

  //Login, Signup & forgot Password
  static const login = 'Login';
  static const email_Address = 'Email Address';
  static const password = 'Password';
  static const new_password = 'New Password';
  static const full_Name = 'Full Name';
  static const mobile_Phone = 'Mobile Phone';
  static const or_sign_in_with_google = 'Or sign in\nwith google';
  static const need_an_account = 'need an account?';
  static const forgot_your_password = 'forgot your password?';
  static const sign_up = 'Sign up';
  static const reset = 'Reset';
  static const already_have_an_account = 'already have an account?';
  static const forgot_Password = 'Forgot Password';
  static const please_enter_your_email_address =
      'Please enter your email address and we will send you a reset email.';
  static const reset_Password = 'Reset Password';
  static const reset_email_sent_to_your_mailbox =
      'Reset email sent to your mailbox.';
  static const send_again = 'Send again';

  //Profile
  static const save = 'Save';

  //AppointmentSummary
  static const yes = 'Yes';
  static const no = 'No';

  //Splash
  static const launch = 'Launch';

  //Dashboard
  static const dashboard = 'Dashboard';
  static const start_a_Call = 'Start a Call';

  //Patient Search
  static const patient_Search = 'Patient Search';
  static const doctor_Search = 'Doctor Search';
  static const search = 'Search';
  static const select = 'Select';

  //Patient info
  static const patient_info = 'Patient information';
  static const text_Consent_Form = 'Text Consent Form';
  static const email_Consent_Form = 'Email Consent Form';
  static const link_Existing_Consent_Form = 'Link Existing Consent Form';

  //Video Calling
  static const consent_Form_Signed = 'Consent Form Signed';
  static const your_appointment_has_ended =
      'Your appointment has ended and your recording has been downloaded';

  //Edit profile
  static const date_of_Birth = 'Date of Birth';
  static const update = 'Update';

  //AppDrawer
  //static const start_a_Call = 'Start a Call';
  static const call_Log = 'Call Log';
  static const consent_Forms = 'Consent Forms';
  static const account = 'Account';
  static const settings = 'Settings';
  static const contact = 'Contact';
  static const logout = 'Logout';

  //Call Logs
  static const date_of_call = 'Date of call:';
  static const time_of_call = 'Time of call:';
  static const end_of_call = 'End of call:';
  static const length_of_call = 'Length of call:';
  static const purpose_of_call = 'Purpose of call:';
  static const follow_up_neeeded = 'Follow up neeeded:';

  //Form
  static const i_agree = 'I Agree';

  //Settings
  static const change_Password = 'Change Password';
  static const support = 'Support';
  static const privacy_Policy = 'Privacy Policy';

  //Accounts
  static const gender = 'Gender';
  static const birthdate = 'Birthdate';
  static const height = 'Height';
  static const weight = 'Weight';
  static const insurance = 'Insurance';
  static const policy = 'Policy #';
  static const edit_account = 'Edit account';

  //Popup Dialogs
  static const ok = 'Ok';
  static const alert = 'Alert';
  static const cancel = 'Cancel';
  static const try_Again = 'Try Again';
  static const password_and_Confirm_Password_Do_not_match =
      'Password and Confirm Password Do not match';
  static const please_Select_Country = 'Please Select Country';
  static const password_Reset_Successfull = 'Password Reset Successfull';

  //errorMessages
  static const error_msg_empty_mobile = 'Please enter a mobile number';
  static const error_msg_invalid_mobile_10_digits =
      'Please enter valid 10 digit phone number';
  static const error_msg_empty_pass = 'Please enter a password';
  static const error_msg_min_length =
      'Password should be minimum of 6 characters';
  static const error_msg_empty_otp = 'Please enter OTP';
  static const error_msg_invalid_otp = 'Please enter vaild OTP';
  static const error_msg_empty_email = 'Please enter a email address';
  static const error_msg_invalid_email = 'Please enter a valid email address';
  static const error_empty_value = 'Please fill field';
  static const error_firstname_empty_value = 'Please enter a first name';
  static const error_lastname_empty_value = 'Please enter a last name';
  static const error_msg_empty_confirm_pass = 'Please enter a confirm password';
  static const error_msg_empty_address = 'Please enter a address';

  //ConsentFormPatient
  static const patient_consent_forms = "Patient Consent \n\n" +
      "Each dental office (the “Dental Office”) you use the eAssist Teledentistry app with (“eAssist Teledentistry”) will be using remote communication technology, including eAssist Teledentistry, to conduct problem-focused evaluations/re-evaluations virtually, to help manage your oral health problem and to determine whether you have a condition that requires immediate in-office treatment (“teledentistry services”).  \n\n" +
      "During the current pandemic the federal government announced that it will not enforce HIPAA regulations (privacy for health records) in connection with medical and dental offices’ good faith provision of medical or dental services using non-public facing audio or video remote communications services. Remote patient consultations may take place over applications that allow video chats such as Apple FaceTime, Facebook Messenger video chat, Google Hangouts, or Skype and may involve or be based on photos or videos taken with smartphones by the patient and transmitted to the Dental Office. (Please do not contact the Dental Office using public-facing services such as Facebook Live, Twitch, or TikTok, which are not permitted by the federal government for this purpose.) \n\n" +
      "The Dental Office takes dental record confidentiality seriously, and is taking measures under the circumstances to protect the information you send. While the Dental Office believes the risk to such confidentiality is not high, it may be greater than it would be if these remote electronic communications were encrypted, which is one of the main HIPAA requirements that is being relaxed during the nationwide COVID-19 public health emergency. Certain major dental plans have announced that they will reimburse dental offices for conducting such remote evaluations, and the Dental Office may submit claims in connection with them.  \n\n" +
      "The Dental Office is using communication technology, such as eAssist Teledentistry, for remote transmission of information to conduct limited problem focused evaluations. While entirely adequate in the vast majority of cases for such limited purposes, these evaluations may not reveal conditions that would be discovered during an office visit or through the use of specialized teledentistry technology.  These limitations and others may render the teledentistry services inadequate or misleading, resulting in incorrect or incomplete evaluation, consultation and treatment.  An in-person consultation with a dentist may still be necessary after the teledentistry services.   \n\n" +
      "You acknowledge and agree that no guarantee or assurance has been made by anyone regarding any specific results or outcome from your participation in any teledentistry consultation or the services provided in connection therewith by the Dental Office or otherwise. \n\n" +
      "You consent to using eAssist Teledentistry or any other non-public remote communication services for teledentistry services.  You specifically consent to the taking and use of video, photographic and radiographic images and oral recordings, and the transmission and retention of these images and recordings through teledentistry services conducted by the Dental Office for purposes of consultation and furthering your oral healthcare, whether via eAssist Teledentistry or any other non-public remote communication services.  You agree not to hold the Dental Office or eAssist, Inc. responsible or liable in any way in connection with using eAssist Teledentistry or any other non-public remote communication services, including without limitation if you experience any negative health conditions or consequences, or if your personal health or other information is compromised or accessed or used by unauthorized parties, through, in connection with, or because of the Dental Office’s use of eAssist Teledentristy or any other non-public remote communication services. You assume the risk of all these things in consenting to use this technology.  The Dental Office is deemed a party to this consent, and eAssist, Inc. is a third-party beneficiary of this consent and may enforce it. \n\n" +
      "You agree that you have been informed of and understand the potential benefits, risks and alternatives of participating in teledentistry services and using eAssist Teledentistry.  Please indicate your understanding of and informed consent to all of the foregoing (including for any minor, as applicable) by clicking “I agree” below. \n\n";
}
