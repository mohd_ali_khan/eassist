class UrlLinksModel {
  int status;
  int errorcode;
  String message;
  UrlLinksData data;

  UrlLinksModel({this.status, this.errorcode, this.message, this.data});

  UrlLinksModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    errorcode = json['errorcode'];
    message = json['message'];
    data =
        json['data'] != null ? new UrlLinksData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['errorcode'] = this.errorcode;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class UrlLinksData {
  String url;
  String doctorUrl;
  String patientUrl;

  UrlLinksData({this.url});

  UrlLinksData.fromJson(Map<String, dynamic> json) {
    url = json['url'];
    doctorUrl = json['doctor_url'];
    patientUrl = json['patient_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['url'] = this.url;
    return data;
  }
}
