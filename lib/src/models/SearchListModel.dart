class SearchListModel {
  Version version;
  int status;
  String message;
  int errorcode;
  List<SearchListData> data;

  SearchListModel(
      {this.version, this.status, this.message, this.errorcode, this.data});

  SearchListModel.fromJson(Map<String, dynamic> json) {
    version =
        json['version'] != null ? new Version.fromJson(json['version']) : null;
    status = json['status'];
    message = json['message'];
    errorcode = json['errorcode'];
    if (json['data'] != null) {
      data = new List<SearchListData>();
      json['data'].forEach((v) {
        data.add(new SearchListData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.version != null) {
      data['version'] = this.version.toJson();
    }
    data['status'] = this.status;
    data['message'] = this.message;
    data['errorcode'] = this.errorcode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Version {
  int status;
  int versioncode;
  String versionmessage;
  String currentVersion;

  Version(
      {this.status,
      this.versioncode,
      this.versionmessage,
      this.currentVersion});

  Version.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    versioncode = json['versioncode'];
    versionmessage = json['versionmessage'];
    currentVersion = json['current_version'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['versioncode'] = this.versioncode;
    data['versionmessage'] = this.versionmessage;
    data['current_version'] = this.currentVersion;
    return data;
  }
}

class SearchListData {
  String totalcount;
  List<Record> record;

  SearchListData({this.totalcount, this.record});

  SearchListData.fromJson(Map<String, dynamic> json) {
    totalcount = json['totalcount'];
    if (json['record'] != null) {
      record = new List<Record>();
      json['record'].forEach((v) {
        record.add(new Record.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalcount'] = this.totalcount;
    if (this.record != null) {
      data['record'] = this.record.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Record {
  String id;
  String fullName;
  String userPic;
  String userPhone;

  Record({this.id, this.fullName, this.userPic, this.userPhone});

  Record.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fullName = json['full_name'];
    userPic = json['user_pic'];
    userPhone = json['user_phone'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['full_name'] = this.fullName;
    data['user_pic'] = this.userPic;
    data['user_phone'] = this.userPhone;
    return data;
  }
}
