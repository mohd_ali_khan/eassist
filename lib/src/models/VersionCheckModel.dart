class VersionCheckModel {
  VersionCheckData data;

  VersionCheckModel({this.data});

  VersionCheckModel.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null
        ? new VersionCheckData.fromJson(json['data'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class VersionCheckData {
  Version version;

  VersionCheckData({this.version});

  VersionCheckData.fromJson(Map<String, dynamic> json) {
    version =
        json['version'] != null ? new Version.fromJson(json['version']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.version != null) {
      data['version'] = this.version.toJson();
    }
    return data;
  }
}

class Version {
  int status;
  int versioncode;
  String versionmessage;
  String currentVersion;
  String appStore;
  String playStore;

  Version(
      {this.status,
      this.versioncode,
      this.versionmessage,
      this.currentVersion,
      this.appStore,
      this.playStore});

  Version.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    versioncode = json['versioncode'];
    versionmessage = json['versionmessage'];
    currentVersion = json['current_version'];
    appStore = json['app_store'];
    playStore = json['play_store'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['versioncode'] = this.versioncode;
    data['versionmessage'] = this.versionmessage;
    data['current_version'] = this.currentVersion;
    data['app_store'] = this.appStore;
    data['play_store'] = this.playStore;
    return data;
  }
}
