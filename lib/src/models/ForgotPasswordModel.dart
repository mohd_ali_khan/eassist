class ForgotPasswordModel {
  Version version;
  int status;
  int errorcode;
  Data data;
  String message;

  ForgotPasswordModel(
      {this.version, this.status, this.errorcode, this.data, this.message});

  ForgotPasswordModel.fromJson(Map<String, dynamic> json) {
    version =
        json['version'] != null ? new Version.fromJson(json['version']) : null;
    status = json['status'];
    errorcode = json['errorcode'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.version != null) {
      data['version'] = this.version.toJson();
    }
    data['status'] = this.status;
    data['errorcode'] = this.errorcode;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class Version {
  int status;
  int versioncode;
  String versionmessage;
  String currentVersion;

  Version(
      {this.status,
      this.versioncode,
      this.versionmessage,
      this.currentVersion});

  Version.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    versioncode = json['versioncode'];
    versionmessage = json['versionmessage'];
    currentVersion = json['current_version'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['versioncode'] = this.versioncode;
    data['versionmessage'] = this.versionmessage;
    data['current_version'] = this.currentVersion;
    return data;
  }
}

class Data {
  Forgot forgot;

  Data({this.forgot});

  Data.fromJson(Map<String, dynamic> json) {
    forgot =
        json['forgot'] != null ? new Forgot.fromJson(json['forgot']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.forgot != null) {
      data['forgot'] = this.forgot.toJson();
    }
    return data;
  }
}

class Forgot {
  String userId;
  String password;

  Forgot({this.userId, this.password});

  Forgot.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    password = json['Password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['Password'] = this.password;
    return data;
  }
}
