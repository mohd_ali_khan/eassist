class LoginModel {
  Version version;
  int status;
  int errorcode;
  String message;
  LoginData data;

  LoginModel(
      {this.version, this.status, this.errorcode, this.message, this.data});

  LoginModel.fromJson(Map<String, dynamic> json) {
    version =
        json['version'] != null ? new Version.fromJson(json['version']) : null;
    status = json['status'];
    errorcode = json['errorcode'];
    message = json['message'];
    data = json['data'] != null ? new LoginData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.version != null) {
      data['version'] = this.version.toJson();
    }
    data['status'] = this.status;
    data['errorcode'] = this.errorcode;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Version {
  int status;
  int versioncode;
  String versionmessage;
  String currentVersion;

  Version(
      {this.status,
      this.versioncode,
      this.versionmessage,
      this.currentVersion});

  Version.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    versioncode = json['versioncode'];
    versionmessage = json['versionmessage'];
    currentVersion = json['current_version'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['versioncode'] = this.versioncode;
    data['versionmessage'] = this.versionmessage;
    data['current_version'] = this.currentVersion;
    return data;
  }
}

class LoginData {
  Login login;

  LoginData({this.login});

  LoginData.fromJson(Map<String, dynamic> json) {
    login = json['login'] != null ? new Login.fromJson(json['login']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.login != null) {
      data['login'] = this.login.toJson();
    }
    return data;
  }
}

class Login {
  String authorization;
  String userId;
  String userEmail;
  String fullName;
  String userRole;
  String userPic;

  Login(
      {this.authorization,
      this.userId,
      this.userEmail,
      this.fullName,
      this.userRole,
      this.userPic});

  Login.fromJson(Map<String, dynamic> json) {
    authorization = json['Authorization'];
    userId = json['user_id'];
    userEmail = json['user_email'];
    fullName = json['full_name'];
    userRole = json['user_role'];
    userPic = json['user_pic'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Authorization'] = this.authorization;
    data['user_id'] = this.userId;
    data['user_email'] = this.userEmail;
    data['full_name'] = this.fullName;
    data['user_role'] = this.userRole;
    data['user_pic'] = this.userPic;
    return data;
  }
}
