class CallLogsModel {
  Version version;
  int status;
  int errorcode;
  String message;
  List<CallLogsData> data;

  CallLogsModel(
      {this.version, this.status, this.errorcode, this.message, this.data});

  CallLogsModel.fromJson(Map<String, dynamic> json) {
    version =
        json['version'] != null ? new Version.fromJson(json['version']) : null;
    status = json['status'];
    errorcode = json['errorcode'];
    message = json['message'];
    if (json['data'] != null) {
      data = new List<CallLogsData>();
      json['data'].forEach((v) {
        data.add(new CallLogsData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.version != null) {
      data['version'] = this.version.toJson();
    }
    data['status'] = this.status;
    data['errorcode'] = this.errorcode;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Version {
  int status;
  int versioncode;
  String versionmessage;
  String currentVersion;

  Version(
      {this.status,
      this.versioncode,
      this.versionmessage,
      this.currentVersion});

  Version.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    versioncode = json['versioncode'];
    versionmessage = json['versionmessage'];
    currentVersion = json['current_version'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['versioncode'] = this.versioncode;
    data['versionmessage'] = this.versionmessage;
    data['current_version'] = this.currentVersion;
    return data;
  }
}

class CallLogsData {
  String totalcount;
  List<LogDetails> logDetails;

  CallLogsData({this.totalcount, this.logDetails});

  CallLogsData.fromJson(Map<String, dynamic> json) {
    totalcount = json['totalcount'];
    if (json['log_details'] != null) {
      logDetails = new List<LogDetails>();
      json['log_details'].forEach((v) {
        logDetails.add(new LogDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalcount'] = this.totalcount;
    if (this.logDetails != null) {
      data['log_details'] = this.logDetails.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class LogDetails {
  String logId;
  String callerUserId;
  String receiverUserId;
  String dtOfCall;
  String timeOfCall;
  String endOfCall;
  String purposeCall;
  String followupNeeded;
  String createdOn;
  String name;
  String number;

  LogDetails(
      {this.logId,
      this.callerUserId,
      this.receiverUserId,
      this.dtOfCall,
      this.timeOfCall,
      this.endOfCall,
      this.purposeCall,
      this.followupNeeded,
      this.createdOn,
      this.name,
      this.number});

  LogDetails.fromJson(Map<String, dynamic> json) {
    logId = json['log_id'];
    callerUserId = json['caller_user_id'];
    receiverUserId = json['receiver_user_id'];
    dtOfCall = json['dt_of_call'];
    timeOfCall = json['time_of_call'];
    endOfCall = json['end_of_call'];
    purposeCall = json['purpose_call'];
    followupNeeded = json['followup_needed'];
    createdOn = json['created_on'];
    name = json['name'];
    number = json['number'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['log_id'] = this.logId;
    data['caller_user_id'] = this.callerUserId;
    data['receiver_user_id'] = this.receiverUserId;
    data['dt_of_call'] = this.dtOfCall;
    data['time_of_call'] = this.timeOfCall;
    data['end_of_call'] = this.endOfCall;
    data['purpose_call'] = this.purposeCall;
    data['followup_needed'] = this.followupNeeded;
    data['created_on'] = this.createdOn;
    data['name'] = this.name;
    data['number'] = this.number;
    return data;
  }
}
