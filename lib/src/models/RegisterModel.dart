class RegisterModel {
  Version version;
  int status;
  int errorcode;
  String message;
  RegisterData data;

  RegisterModel(
      {this.version, this.status, this.errorcode, this.message, this.data});

  RegisterModel.fromJson(Map<String, dynamic> json) {
    version =
        json['version'] != null ? new Version.fromJson(json['version']) : null;
    status = json['status'];
    errorcode = json['errorcode'];
    message = json['message'];
    data =
        json['data'] != null ? new RegisterData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.version != null) {
      data['version'] = this.version.toJson();
    }
    data['status'] = this.status;
    data['errorcode'] = this.errorcode;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Version {
  int status;
  int versioncode;
  String versionmessage;
  String currentVersion;

  Version(
      {this.status,
      this.versioncode,
      this.versionmessage,
      this.currentVersion});

  Version.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    versioncode = json['versioncode'];
    versionmessage = json['versionmessage'];
    currentVersion = json['current_version'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['versioncode'] = this.versioncode;
    data['versionmessage'] = this.versionmessage;
    data['current_version'] = this.currentVersion;
    return data;
  }
}

class RegisterData {
  Register register;

  RegisterData({this.register});

  RegisterData.fromJson(Map<String, dynamic> json) {
    register = json['register'] != null
        ? new Register.fromJson(json['register'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.register != null) {
      data['register'] = this.register.toJson();
    }
    return data;
  }
}

class Register {
  String userId;

  Register({this.userId});

  Register.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    return data;
  }
}
