class UserProfileModel {
  Version version;
  int status;
  int errorcode;
  UserProfileData data;
  String message;

  UserProfileModel(
      {this.version, this.status, this.errorcode, this.data, this.message});

  UserProfileModel.fromJson(Map<String, dynamic> json) {
    version =
        json['version'] != null ? new Version.fromJson(json['version']) : null;
    status = json['status'];
    errorcode = json['errorcode'];
    data = json['data'] != null
        ? new UserProfileData.fromJson(json['data'])
        : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.version != null) {
      data['version'] = this.version.toJson();
    }
    data['status'] = this.status;
    data['errorcode'] = this.errorcode;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class Version {
  int status;
  int versioncode;
  String versionmessage;
  String currentVersion;

  Version(
      {this.status,
      this.versioncode,
      this.versionmessage,
      this.currentVersion});

  Version.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    versioncode = json['versioncode'];
    versionmessage = json['versionmessage'];
    currentVersion = json['current_version'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['versioncode'] = this.versioncode;
    data['versionmessage'] = this.versionmessage;
    data['current_version'] = this.currentVersion;
    return data;
  }
}

class UserProfileData {
  Profile profile;

  UserProfileData({this.profile});

  UserProfileData.fromJson(Map<String, dynamic> json) {
    profile =
        json['profile'] != null ? new Profile.fromJson(json['profile']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.profile != null) {
      data['profile'] = this.profile.toJson();
    }
    return data;
  }
}

class Profile {
  String id;
  String userEmail;
  String fullName;
  String countryCode;
  String userPhone;
  String dob;
  String gender;
  String userRole;
  String height;
  String weight;
  String insurance;
  String policy;
  String userPic;

  Profile(
      {this.id,
      this.userEmail,
      this.fullName,
      this.countryCode,
      this.userPhone,
      this.dob,
      this.gender,
      this.userRole,
      this.height,
      this.weight,
      this.insurance,
      this.policy,
      this.userPic});

  Profile.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userEmail = json['user_email'];
    fullName = json['full_name'];
    countryCode = json['country_code'];
    userPhone = json['user_phone'];
    dob = json['dob'];
    gender = json['gender'];
    userRole = json['user_role'];
    height = json['height'];
    weight = json['weight'];
    insurance = json['insurance'];
    policy = json['policy'];
    userPic = json['user_pic'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_email'] = this.userEmail;
    data['full_name'] = this.fullName;
    data['country_code'] = this.countryCode;
    data['user_phone'] = this.userPhone;
    data['dob'] = this.dob;
    data['gender'] = this.gender;
    data['user_role'] = this.userRole;
    data['height'] = this.height;
    data['weight'] = this.weight;
    data['insurance'] = this.insurance;
    data['policy'] = this.policy;
    data['user_pic'] = this.userPic;
    return data;
  }
}
